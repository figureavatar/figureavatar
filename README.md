# FigureAvatar
<img width="300px" src="Textures/logo.png"></img>
# ふぃぎゅあばたープロジェクトについて
【積みアバターを無くそう！】  
アバター向け3Dモデルの供給増加に伴い、巷では積みアバターがバーチャル社会現象となっています。  
アバターはたくさんあっても、身体は1つ…  
そんな現状に一石を投じるべく「”動く声付きフィギュア”としてアバターを楽しむ」規格をリリースします。  

[ふぃぎゅあばたープロモーション動画](https://youtu.be/C8b8RoFsVlU)

本unitypackage(無料)に含まれるprefabに、  
+ 3Dモデル(アバター)  
+ モーション  
+ ボイス  

をセットしてVRChatのワールドとしてアップすると、触る箇所によって反応が異なるアバターのフィギュアを楽しむことができます！  
ふぃぎゅあばたー のロゴと名称はどなたでも自由に利用することが出来ます。  
  
ふぃぎゅあばたー は、積みアバターの解決のみならず、「対応モーション」や「対応ボイス」の需要が生まれることで  
モーションファイルやボイスファイルの収益化機会の創造も狙っています。
 
  
# 使い方
[こちらを御覧ください](https://gitlab.com/figureavatar/figureavatar/-/wikis/How-to-use)

# ライセンス
本プロジェクト成果物のライセンスは以下です。  
These codes are licensed under CC BY 4.0.
https://creativecommons.org/licenses/by/4.0/deed.ja

### 利用条件
以下を記載してください。
```
ふぃぎゅあばたー
配布場所 https://gitlab.com/figureavatar/figureavatar
```
  
各アバターや、アセット、素材のライセンスは、各ライセンスに従います。  
ライセンスにご注意の上ご利用ください。
