﻿using UnityEngine;

namespace FigureAvatar
{
    public class FigureAvatarConfigInfo : ScriptableObject
    {
        public ConfigInfo configInfo = new ConfigInfo();
        public GameObject avatarObj;
        public bool avatarObjFlag;
        public bool typesFlag;
        public ClipFold idleFold;
        public ClipFold approachFold;
        public ClipFold boneFold;
    }
}
