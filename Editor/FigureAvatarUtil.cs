﻿using UnityEngine;
using UnityEditor;
using System.Linq;
using System.IO;

namespace FigureAvatar
{
    internal static class FigureAvatarUtil
    {
        internal const string saveFolderPath = "Assets/FigureAvatarPrefabs";
        internal const string controllersFolderPath = "Assets/FigureAvatarPrefabs/Controllers";

        internal static string pressAnimatorPath
        {
            get
            {
                return GetMainFolderPath() + "/Gimmicks/Animations/Press.controller";
            }
        }

        internal static string pressManualAnimatorPath
        {
            get
            {
                return GetMainFolderPath() + "/Gimmicks/Animations/PressManual.controller";
            }
        }

        internal static string lookatManagerAnimatorPath
        {
            get
            {
                return GetMainFolderPath() + "/Gimmicks/Animations/LookatManager.controller";
            }
        }

        internal static string soundlessAudioClipPath
        {
            get
            {
                return GetMainFolderPath() + "/Gimmicks/Audio/soundless.ogg";
            }
        }

        internal static FigureAvatarConfigInfo ConfigLoad()
        {
            return AssetLoad<FigureAvatarConfigInfo>(typeof(FigureAvatarConfigInfo).Name + ".asset", typeof(FigureAvatarConfigInfo).Name + ".cs", "Config用スクリプトが見つかりませんでした。再インポートしてください。");
        }

        internal static VersionInfo VersionLoad()
        {
            return AssetLoad<VersionInfo>(typeof(VersionInfo).Name + ".asset", typeof(VersionInfo).Name + ".cs", "Version用スクリプトが見つかりませんでした。再インポートしてください。");
        }

        internal static PresetInfo PresetLoad()
        {
            return AssetLoad<PresetInfo>(typeof(PresetInfo).Name + ".asset", typeof(PresetInfo).Name + ".cs", "Preset用スクリプトが見つかりませんでした。再インポートしてください。");
        }

        internal static string GetMainFolderPath()
        {
            return AssetDatabase.FindAssets("l:FigureAvatarFolder").Select(g => AssetDatabase.GUIDToAssetPath(g)).Where(p => p != null).FirstOrDefault();
        }

        internal static void CreateFolders()
        {
            if (!Directory.Exists(saveFolderPath))
            {
                Directory.CreateDirectory(saveFolderPath);
            }

            if (!Directory.Exists(controllersFolderPath))
            {
                Directory.CreateDirectory(controllersFolderPath);
            }
        }

        internal static void CreateDefaultPreset()
        {
            PresetInfo preset = PresetLoad();

            PresetItem item = new PresetItem();
            item.presetName = "Reset";
            item.config.boneInfoList.Add(new BoneInfo(HumanBodyBones.Head));
            item.config.boneInfoList.Add(new BoneInfo(HumanBodyBones.Spine));
            item.config.boneInfoList.Add(new BoneInfo(HumanBodyBones.LeftHand));
            item.config.boneInfoList.Add(new BoneInfo(HumanBodyBones.RightHand));
            item.config.saveFolder = AssetDatabase.LoadAssetAtPath<DefaultAsset>(saveFolderPath);

            if (preset.presetList.Count == 0 || JsonUtility.ToJson(preset.presetList[0]) != JsonUtility.ToJson(item))
            {
                if (preset.presetList.Count == 0)
                {
                    preset.presetList.Add(item);
                }
                else if (JsonUtility.ToJson(preset.presetList[0]) != JsonUtility.ToJson(item))
                {
                    preset.presetList[0] = item;
                }
                AssetDatabase.SaveAssets();
                EditorUtility.SetDirty(preset);
            }
        }

        internal static T AssetLoad<T>(string assetName, string assetScriptName, string errorMsg) where T : ScriptableObject
        {
            string assetPath = GetMainFolderPath() + "/Editor/" + assetName;
            string scriptablePath = GetMainFolderPath() + "/Editor/" + assetScriptName;
            if (!File.Exists(scriptablePath))
            {
                ErrorMessage(errorMsg);
                return null;
            }
            if (!File.Exists(assetPath))
            {
                ScriptableObject scriptableObj = ScriptableObject.CreateInstance(typeof(T));
                AssetDatabase.CreateAsset(scriptableObj, assetPath);
            }
            return AssetDatabase.LoadAssetAtPath(assetPath, typeof(T)) as T;
        }

        internal static bool ErrorMessage(string msg)
        {
            return MessageBox("エラー", msg, MessageType.OK);
        }

        internal static bool MessageBox(string title, string msg, MessageType type)
        {
            switch (type)
            {
                case MessageType.OK:
                    return EditorUtility.DisplayDialog(title, msg, "OK");
                case MessageType.YesNo:
                    return EditorUtility.DisplayDialog(title, msg, "はい", "いいえ");
            }
            return false;
        }

        internal static class Styles
        {
            public static GUIStyle ButtonMiddle
            {
                get
                {
                    GUIStyle style = new GUIStyle(GUI.skin.button);
                    style.fixedHeight = 20;
                    return style;
                }
            }

            public static GUIStyle ButtonBig
            {
                get
                {
                    GUIStyle style = new GUIStyle(GUI.skin.button);
                    style.fixedHeight = 30;
                    return style;
                }
            }

            public static GUIStyle PopupMiddle
            {
                get
                {
                    GUIStyle style = new GUIStyle("Popup");
                    style.fixedHeight = 18.4f;
                    return style;
                }
            }

            public static bool Foldout(string title, bool display)
            {
                GUIStyle style = new GUIStyle("ShurikenModuleTitle");
                style.margin = new RectOffset(0, 0, 8, 0);
                style.font = new GUIStyle(EditorStyles.label).font;
                style.border = new RectOffset(15, 7, 4, 4);
                style.fixedHeight = 22;
                style.contentOffset = new Vector2(20, -2);
                Rect rect = GUILayoutUtility.GetRect(16, 22, style);
                GUI.Box(rect, title, style);
                Event e = Event.current;
                Rect toggleRect = new Rect(rect.x + 4, rect.y + 2, 13, 13);
                if (e.type == EventType.Repaint)
                {
                    EditorStyles.foldout.Draw(toggleRect, false, false, display, false);
                }
                if (e.type == EventType.MouseDown && rect.Contains(e.mousePosition))
                {
                    display = !display;
                    e.Use();
                }
                return display;
            }
        }

        internal enum MessageType
        {
            OK,
            YesNo
        }
    }
}