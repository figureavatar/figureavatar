﻿using UnityEditor;
using UnityEngine;

namespace FigureAvatar
{
    [CustomEditor(typeof(FigureAvatarInfo))]
    internal class FigureAvatarInfoEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            if (GUILayout.Button("Open Setup Window"))
            {
                OpenSetupWindow(true);
            }
        }

        private void OpenSetupWindow(bool editMode)
        {
            FigureAvatarInfo figureInfo = target as FigureAvatarInfo;

            FigureAvatarSetupWindow window = FigureAvatarSetupWindow.ShowSetupWindow();
            window.editMode = editMode;
            window.config.configInfo.boneInfoList = figureInfo.config.boneInfoList;
            window.config.configInfo.idleInfo = figureInfo.config.idleInfo;
            window.config.configInfo.approachInfo = figureInfo.config.approachInfo;
            window.config.avatarObj = figureInfo.gameObject;
            window.config.configInfo.eventObjs = figureInfo.config.eventObjs;
        }
    }
}
