﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;
using VRCSDK2;
using UnityEditorInternal;
using UnityEngine.UI;

namespace FigureAvatar
{
    internal class FigureAvatarSetupWindow : EditorWindow
    {
        internal static readonly Vector2 minWindowSize = new Vector2(276, 184);

        [SerializeField]
        internal bool editMode = false;
        [SerializeField]
        internal List<HumanBodyBones> activeBoneList = new List<HumanBodyBones>();
        [SerializeField]
        internal FigureAvatarConfigInfo config;
        [SerializeField]
        internal PresetInfo preset;
        [SerializeField]
        internal VersionInfo versionInfo;

        private Dictionary<System.Type, ReorderInfo> reorderLists = new Dictionary<System.Type, ReorderInfo>();
        private Vector2 scrollPosition;
        private StandardAssetTypes types = new StandardAssetTypes();

        private class StandardAssetTypes
        {
            public System.Type lookatTargetType = null;
            public System.Type autoCamType = null;
            public System.Type timedObjectActivatorType = null;
        }

        private Navigation nav
        {
            get
            {
                Navigation nav = new Navigation();
                nav.mode = Navigation.Mode.None;
                return nav;
            }
        }

        private class ReorderInfo
        {
            public ReorderableList mainReorderList;
            public ReorderableList clipReorderList;
        }

        [MenuItem("FigureAvatar/Setup Window")]
        internal static FigureAvatarSetupWindow ShowSetupWindow()
        {
            Dictionary<string, int> dic = new Dictionary<string, int>();
            var nList = (new int[5]).ToList();
            foreach (var keypair in dic)
            {
                dic[keypair.Key]--;
            }
            string[] maxKeys = dic.OrderByDescending(v => v.Value).OrderBy(v => int.Parse(v.Key)).Select(s => s.Key).ToArray();
            FigureAvatarSetupWindow window = GetWindow<FigureAvatarSetupWindow>();
            window.minSize = minWindowSize;
            window.editMode = false;
            window.Init();
            return window;
        }

        internal void Init()
        {
            versionInfo = FigureAvatarUtil.VersionLoad();
            config = FigureAvatarUtil.ConfigLoad();
            preset = FigureAvatarUtil.PresetLoad();

            titleContent.text = "Setup Window " + versionInfo.version;

            FigureAvatarUtil.CreateFolders();

            if (preset != null)
            {
                FigureAvatarUtil.CreateDefaultPreset();
            }

            StandardAssetLoad();
            SetReorderList();
            AvatarChanged();
        }

        private void OnFocus()
        {
            FigureAvatarUtil.CreateFolders();
            FigureAvatarUtil.CreateDefaultPreset();
            FigureAvatarVersion.CompatibleConvert();
        }

        private void SetReorderList()
        {
            if (config.configInfo.boneInfoList != null)
            {
                ReorderInfo reorderInfo = ReorderInfoCheck(typeof(BoneInfo));
                ReorderableList reorderList = new ReorderableList(config.configInfo.boneInfoList, typeof(BoneInfo));
                reorderList.drawHeaderCallback += (rect) => EditorGUI.LabelField(rect, "Bones");
                reorderList.drawElementCallback += (rect, index, isActive, isFocused) =>
                {
                    if (reorderList.list.Count <= index)
                    {
                        return;
                    }

                    HumanBodyBones tmp_bone = HumanBodyBones.Hips;
                    float tmp_coliderSize;
                    Vector3 tmp_offset;
                    BoneInfo boneInfo = reorderList.list[index] as BoneInfo;
                    rect.height -= 4;
                    rect.y += 2;
                    float x = rect.x;
                    float w = rect.width;
                    bool rectSmallWidthFlag = rect.width < 350;
                    string colLabel = rectSmallWidthFlag ? "Col" : "Collider Size";
                    float colLabelSize = GUIStyle.none.CalcSize(new GUIContent(colLabel)).x + 5;
                    float offsetSize = rectSmallWidthFlag ? 120 : 160;
                    float colFieldSize = rectSmallWidthFlag ? 30 : 40;

                    if (boneInfo == null)
                    {
                        return;
                    }

                    EditorGUI.BeginChangeCheck();
                    {
                        w = rect.width - offsetSize - colLabelSize - colFieldSize;
                        int selectBoneIndex = EditorGUI.Popup(new Rect(x, rect.y, w, rect.height), GetHumanBoneToListIndex(boneInfo.bone, activeBoneList), activeBoneList.Select(b => b.ToString()).ToArray());
                        x += w;

                        w = offsetSize;
                        tmp_offset = EditorGUI.Vector3Field(new Rect(x, rect.y, w, rect.height), "", boneInfo.offset);
                        x += w;

                        if (activeBoneList.Count > selectBoneIndex)
                        {
                            tmp_bone = activeBoneList[selectBoneIndex];
                        }

                        w = colLabelSize;
                        EditorGUI.LabelField(new Rect(x, rect.y, w, rect.height), colLabel);
                        x += w;

                        w = colFieldSize;
                        tmp_coliderSize = EditorGUI.FloatField(new Rect(x, rect.y, w, rect.height), boneInfo.colSize);
                        x += w;
                    }
                    if (EditorGUI.EndChangeCheck())
                    {
                        boneInfo.bone = tmp_bone;
                        boneInfo.offset = tmp_offset;
                        boneInfo.colSize = tmp_coliderSize;
                    }
                };
                reorderList.onSelectCallback += (list) =>
                {
                    BoneInfo boneInfo = list.list[list.index] as BoneInfo;
                    SetClipReorderList(typeof(BoneInfo), boneInfo);
                };
                reorderList.onAddCallback += (list) =>
                {
                    BoneInfo boneInfo = new BoneInfo();
                    config.configInfo.boneInfoList.Add(boneInfo);
                    reorderList.list = config.configInfo.boneInfoList;
                    reorderList.index = reorderList.count - 1;
                    reorderList.onSelectCallback.Invoke(list);
                };
                reorderList.onRemoveCallback += (list) =>
                {
                    config.configInfo.boneInfoList.RemoveAt(list.index);
                    reorderList.list = config.configInfo.boneInfoList;
                    if (reorderList.index > reorderList.count - 1)
                        reorderList.index = reorderList.count - 1;
                    reorderList.onSelectCallback.Invoke(list);
                };

                reorderInfo.mainReorderList = reorderList;
                reorderInfo.clipReorderList = null;
            }

            if (config.configInfo.idleInfo != null)
            {
                System.Type type = typeof(IdleInfo);
                ReorderInfo reorderInfo = ReorderInfoCheck(type);
                ReorderableList reorderList = new ReorderableList(new IdleInfo[] { config.configInfo.idleInfo, config.configInfo.idleInfo }, typeof(IdleInfo));
                reorderList.drawHeaderCallback += (rect) => EditorGUI.LabelField(rect, "Idle");
                reorderList.drawElementCallback += (rect, index, isActive, isFocused) =>
                {
                    rect.height -= 4;
                    rect.y += 2;

                    if (index == 0)
                    {
                        AnimationClip tmp_defaultAnimClip;

                        EditorGUI.BeginChangeCheck();
                        {
                            float clipFieldWidth = rect.width - 60;

                            EditorGUI.LabelField(new Rect(rect.x, rect.y, 50, rect.height), "Default");
                            tmp_defaultAnimClip = EditorGUI.ObjectField(new Rect(rect.x + 60, rect.y, clipFieldWidth, rect.height), config.configInfo.idleInfo.defaultAnimClip, typeof(AnimationClip), true) as AnimationClip;
                        }
                        if (EditorGUI.EndChangeCheck())
                        {
                            config.configInfo.idleInfo.defaultAnimClip = tmp_defaultAnimClip;
                        }
                    }
                    else if (index == 1)
                    {
                        float tmp_interval;
                        float tmp_random;

                        EditorGUI.BeginChangeCheck();
                        {
                            float clipFieldWidth = rect.width - 60 - 60 - 50;

                            EditorGUI.LabelField(new Rect(rect.x, rect.y, 50, rect.height), "Interval");
                            tmp_interval = EditorGUI.FloatField(new Rect(rect.x + 60, rect.y, clipFieldWidth, rect.height), config.configInfo.idleInfo.interval);
                            EditorGUI.LabelField(new Rect(rect.x + (clipFieldWidth + 10) + 60, rect.y, 50, rect.height), "Random");
                            tmp_random = EditorGUI.FloatField(new Rect(rect.x + (clipFieldWidth + 10) + 60 + 60, rect.y, 40, rect.height), config.configInfo.idleInfo.random);
                        }
                        if (EditorGUI.EndChangeCheck())
                        {
                            config.configInfo.idleInfo.interval = tmp_interval;
                            config.configInfo.idleInfo.random = tmp_random;
                        }
                    }
                };
                reorderList.onAddCallback = null;
                reorderList.onRemoveCallback = null;
                reorderList.drawFooterCallback = (rect) => { };
                reorderInfo.mainReorderList = reorderList;
                SetClipReorderList(type, config.configInfo.idleInfo);
            }

            if (config.configInfo.approachInfo != null)
            {
                System.Type type = typeof(ApproachInfo);
                ReorderInfo reorderInfo = ReorderInfoCheck(type);
                ReorderableList reorderList = new ReorderableList(new ApproachInfo[] { config.configInfo.approachInfo }, typeof(ApproachInfo));
                reorderList.drawHeaderCallback += (rect) => EditorGUI.LabelField(rect, "Radius");
                reorderList.drawElementCallback += (rect, index, isActive, isFocused) =>
                {
                    float tmp_radius;
                    rect.height -= 4;
                    rect.y += 2;

                    EditorGUI.BeginChangeCheck();
                    {
                        tmp_radius = EditorGUI.FloatField(rect, config.configInfo.approachInfo.radius);
                    }
                    if (EditorGUI.EndChangeCheck())
                    {
                        config.configInfo.approachInfo.radius = tmp_radius;
                    }
                };
                reorderList.onAddCallback = null;
                reorderList.onRemoveCallback = null;
                reorderList.drawFooterCallback = (rect) => { };
                reorderInfo.mainReorderList = reorderList;
                SetClipReorderList(type, config.configInfo.approachInfo);
            }
        }

        private void SetClipReorderList(System.Type type, InfoBase infoBase)
        {
            ReorderInfo reorderInfo = ReorderInfoCheck(type);
            ReorderableList reorderList = new ReorderableList(infoBase.clipList, typeof(ClipInfo));
            reorderList.drawHeaderCallback += (rect) =>
            {
                bool tmp_autoRandom;

                Event e = Event.current;
                if (rect.Contains(e.mousePosition))
                {
                    if (e.type == EventType.DragUpdated)
                    {
                        DragAndDrop.visualMode = DragAndDropVisualMode.Copy;
                    }

                    if (e.type == EventType.DragPerform)
                    {
                        bool changeFlag = false;
                        foreach (Object obj in DragAndDrop.objectReferences)
                        {
                            if (obj.GetType() == typeof(AnimationClip))
                            {
                                AnimationClip clip = obj as AnimationClip;
                                bool notNullFlag = true;
                                for (int i = 0; i < infoBase.clipList.Count; i++)
                                {
                                    if (infoBase.clipList[i].animClip == null)
                                    {
                                        infoBase.clipList[i].animClip = clip;
                                        notNullFlag = false;
                                        break;
                                    }
                                }
                                if (notNullFlag)
                                {
                                    changeFlag = true;
                                    infoBase.clipList.Add(new ClipInfo(clip));
                                }
                            }
                            else if (obj.GetType() == typeof(AudioClip))
                            {
                                AudioClip clip = obj as AudioClip;
                                bool notNullFlag = true;
                                for (int i = 0; i < infoBase.clipList.Count; i++)
                                {
                                    if (infoBase.clipList[i].audioClip == null)
                                    {
                                        infoBase.clipList[i].audioClip = clip;
                                        notNullFlag = false;
                                        break;
                                    }
                                }
                                if (notNullFlag)
                                {
                                    changeFlag = true;
                                    infoBase.clipList.Add(new ClipInfo(clip));
                                }
                            }
                        }
                        if (changeFlag)
                        {
                            reorderList.onChangedCallback.Invoke(reorderList);
                        }
                    }
                }

                EditorGUI.LabelField(new Rect(rect.x, rect.y, rect.width - 120 - 20, rect.height), "Clips");
                EditorGUI.LabelField(new Rect(rect.x + (rect.width - 120 - 20), rect.y, 120, rect.height), "Auto Randomize");
                EditorGUI.BeginChangeCheck();
                {
                    tmp_autoRandom = EditorGUI.Toggle(new Rect(rect.x + (rect.width - 20), rect.y, 20, rect.height), infoBase.autoRandom);
                }
                if (EditorGUI.EndChangeCheck())
                {
                    infoBase.autoRandom = tmp_autoRandom;
                    if (infoBase.autoRandom)
                    {
                        reorderList.list = AutoRandom(reorderList.list as List<ClipInfo>);
                    }
                }
            };
            reorderList.drawElementCallback += (rect, index, isActive, isFocused) =>
            {
                rect.height -= 4;
                rect.y += 2;
                float x = rect.x;
                float w = rect.width;
                bool rectSmallWidthFlag = rect.width < 350;
                string randomLabel = rectSmallWidthFlag ? "Rand" : "Random";
                float randomLabelSize = GUIStyle.none.CalcSize(new GUIContent(randomLabel)).x + 5;
                float randomFieldSize = rectSmallWidthFlag ? 30 : 40;

                ClipInfo tmp_clipInfo = new ClipInfo();
                float tmp_random;
                bool tmp_lookFlag = false;
                ClipInfo clipInfo = reorderList.list[index] as ClipInfo;

                EditorGUI.BeginChangeCheck();
                {
                    w = (rect.width - 30 - 10 - 5 - randomLabelSize - randomFieldSize) / 2;
                    tmp_clipInfo.animClip = EditorGUI.ObjectField(new Rect(x, rect.y, w, rect.height), clipInfo.animClip, typeof(AnimationClip), true) as AnimationClip;
                    x += w;
                    tmp_clipInfo.audioClip = EditorGUI.ObjectField(new Rect(x, rect.y, w, rect.height), clipInfo.audioClip, typeof(AudioClip), true) as AudioClip;
                    x += w;

                    if (config.typesFlag)
                    {
                        EditorGUI.LabelField(new Rect(x, rect.y, 30, rect.height), "Look");
                    }
                    x += 30;
                    if (config.typesFlag)
                    {
                        tmp_lookFlag = EditorGUI.Toggle(new Rect(x, rect.y, 10, rect.height), clipInfo.lookFlag);
                    }
                    x += 10 + 5;

                    w = randomLabelSize;
                    EditorGUI.LabelField(new Rect(x, rect.y, w, rect.height), "Random");
                    x += w;

                    w = randomFieldSize;
                    tmp_random = EditorGUI.FloatField(new Rect(x, rect.y, w, rect.height), clipInfo.random);
                    x += w;
                }
                if (EditorGUI.EndChangeCheck())
                {
                    clipInfo.animClip = tmp_clipInfo.animClip;
                    clipInfo.audioClip = tmp_clipInfo.audioClip;
                    clipInfo.random = tmp_random;
                    clipInfo.lookFlag = tmp_lookFlag;
                }
            };
            reorderList.onChangedCallback += (list) =>
            {
                if (infoBase.autoRandom)
                {
                    reorderList.list = AutoRandom(reorderList.list as List<ClipInfo>);
                    infoBase.clipList = reorderList.list as List<ClipInfo>;
                }
            };
            reorderList.onAddCallback += (list) =>
            {
                ClipInfo clipInfo = new ClipInfo();
                if (!infoBase.autoRandom)
                {
                    clipInfo.random = 0;
                }
                infoBase.clipList.Add(clipInfo);
                reorderList.list = infoBase.clipList;
            };

            reorderInfo.clipReorderList = reorderList;
        }

        private List<ClipInfo> AutoRandom(List<ClipInfo> list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                list[i].random = 1.0f / list.Count;
            }

            return list;
        }

        private void OnGUI()
        {
            try
            {
                ReorderInfo reorderInfo = ReorderInfoCheck(typeof(BoneInfo));

                if (reorderInfo == null || reorderInfo.mainReorderList == null || config == null || preset == null)
                {
                    Init();
                }
                
                if (reorderInfo.mainReorderList == null)
                {
                    reorderInfo = ReorderInfoCheck(typeof(BoneInfo));
                }

                if (versionInfo != null)
                {
                    versionInfo.hideFlags |= HideFlags.NotEditable | HideFlags.DontSave;
                }

                if (config != null)
                {
                    config.hideFlags |= HideFlags.NotEditable | HideFlags.DontSave;
                }

                if (preset != null)
                {
                    preset.hideFlags |= HideFlags.NotEditable | HideFlags.DontSave;
                }

                if (reorderInfo.mainReorderList.list == null)
                {
                    if (config != null)
                    {
                        reorderInfo.mainReorderList.list = config.configInfo.boneInfoList;
                    }
                }

                GameObject tmp_avatarObj;
                DefaultAsset tmp_saveFolder;
                string tmp_prefabName;
                int tmp_preset;

                if (preset.selectPresetIndex < 0)
                {
                    preset.selectPresetIndex = 0;
                }

                if (preset.selectPresetIndex > preset.presetList.Count - 1)
                {
                    preset.selectPresetIndex = preset.presetList.Count - 1;
                }

                EditorGUILayout.Space();
                EditorGUI.BeginChangeCheck();
                {
                    EditorGUILayout.BeginHorizontal();
                    {
                        bool disableFlag = preset.presetList.Count <= 1 || preset.selectPresetIndex == 0;

                        EditorGUILayout.LabelField("Preset:", GUILayout.Width(50));
                        tmp_preset = EditorGUILayout.Popup(preset.selectPresetIndex, preset.presetList.Select(l => l.presetName).ToArray(), FigureAvatarUtil.Styles.PopupMiddle);

                        if (GUILayout.Button("New", GUILayout.Width(50)))
                        {
                            NewPresetWindow.ShowWindow(this);
                        }

                        EditorGUI.BeginDisabledGroup(disableFlag);
                        {
                            if (GUILayout.Button("Set", GUILayout.Width(50)))
                            {
                                if (FigureAvatarUtil.MessageBox("Warning!", "選択中のプリセット「" + preset.presetList[preset.selectPresetIndex].presetName + "」を現在の設定で上書きしますか？", FigureAvatarUtil.MessageType.YesNo))
                                {
                                    preset.presetList[preset.selectPresetIndex].config = config.configInfo.Clone();
                                    AssetDatabase.SaveAssets();
                                    EditorUtility.SetDirty(preset);
                                }
                            }
                        }
                        EditorGUI.EndDisabledGroup();

                        if (GUILayout.Button("Use", GUILayout.Width(50)))
                        {
                            if (FigureAvatarUtil.MessageBox("Warning!", "現在の設定が破棄されますがよろしいですか？", FigureAvatarUtil.MessageType.YesNo))
                            {
                                config.configInfo = preset.presetList[preset.selectPresetIndex].config.Clone();
                                SetReorderList();
                                AssetDatabase.SaveAssets();
                                EditorUtility.SetDirty(config);
                            }
                        }

                        EditorGUI.BeginDisabledGroup(disableFlag);
                        {
                            if (GUILayout.Button("Delete", GUILayout.Width(50)))
                            {
                                if (FigureAvatarUtil.MessageBox("Warning!", "選択中のプリセット「" + preset.presetList[preset.selectPresetIndex].presetName + "」を削除しますか？", FigureAvatarUtil.MessageType.YesNo))
                                {
                                    preset.presetList.RemoveAt(preset.selectPresetIndex);
                                    preset.selectPresetIndex--;
                                    AssetDatabase.SaveAssets();
                                    EditorUtility.SetDirty(preset);
                                }
                            }
                        }
                        EditorGUI.EndDisabledGroup();
                    }
                    EditorGUILayout.EndHorizontal();
                }
                if (EditorGUI.EndChangeCheck())
                {
                    preset.selectPresetIndex = tmp_preset;
                }

                EditorGUILayout.Space();
                EditorGUI.BeginChangeCheck();
                {
                    tmp_avatarObj = EditorGUILayout.ObjectField("Avatar:", config.avatarObj, typeof(GameObject), true) as GameObject;
                }
                if (EditorGUI.EndChangeCheck())
                {
                    config.avatarObj = tmp_avatarObj;
                    config.avatarObjFlag = tmp_avatarObj == null ? false : true;
                    editMode = false;
                    AvatarChanged();
                }

                EditorGUI.BeginChangeCheck();
                {
                    EditorGUILayout.BeginVertical(GUI.skin.box);
                    {
                        scrollPosition = EditorGUILayout.BeginScrollView(scrollPosition);
                        {
                            EditorGUILayout.BeginHorizontal();
                            {
                                GUILayout.Space(5);
                                EditorGUILayout.BeginVertical();
                                {
                                    EditorGUILayout.Space();
                                    DrawReorderList("Idle", typeof(IdleInfo), config.idleFold);

                                    EditorGUILayout.Space();
                                    DrawReorderList("Approach", typeof(ApproachInfo), config.approachFold);

                                    EditorGUILayout.Space();
                                    DrawReorderList("Interact", typeof(BoneInfo), config.boneFold);

                                    if (config.configInfo.boneInfoList != null && config.configInfo.boneInfoList.Count > 0)
                                    {
                                        int clipMax = config.configInfo.boneInfoList.Select(l => l.clipList.Count).Max();
                                        if (reorderInfo != null)
                                        {
                                            int count = reorderInfo.clipReorderList == null ? 0 : reorderInfo.clipReorderList.count;

                                            if (count < clipMax)
                                            {
                                                GUILayout.Space((clipMax - (count == 0 ? 1 : count)) * 21);
                                            }

                                            if (reorderInfo.clipReorderList == null)
                                            {
                                                GUILayout.Space(65);
                                            }
                                        }
                                    }
                                    GUILayout.Space(21);
                                    EditorGUILayout.Space();
                                }
                                EditorGUILayout.EndVertical();
                                GUILayout.Space(5);
                            }
                            EditorGUILayout.EndHorizontal();
                            GUILayout.FlexibleSpace();
                        }
                        EditorGUILayout.EndScrollView();
                    }
                    EditorGUILayout.EndVertical();
                    EditorGUI.BeginDisabledGroup(editMode);
                    {
                        tmp_saveFolder = EditorGUILayout.ObjectField("Save Folder:", config.configInfo.saveFolder, typeof(DefaultAsset), false) as DefaultAsset;
                        if (!AssetDatabase.IsValidFolder(AssetDatabase.GetAssetPath(tmp_saveFolder)))
                        {
                            tmp_saveFolder = null;
                        }
                        tmp_prefabName = EditorGUILayout.TextField("Name:", config.configInfo.prefabName);
                    }
                    EditorGUI.EndDisabledGroup();
                    EditorGUI.BeginDisabledGroup(config.avatarObj == null || config.configInfo.saveFolder == null || tmp_prefabName == "");
                    {
                        if (GUILayout.Button("ふぃぎゅあばたーを" + (editMode ? "編集" : "作成"), FigureAvatarUtil.Styles.ButtonBig))
                        {
                            GenerateAvatar();
                        }
                    }
                    EditorGUI.EndDisabledGroup();
                    EditorGUILayout.Space();
                }
                if (EditorGUI.EndChangeCheck())
                {
                    config.configInfo.saveFolder = tmp_saveFolder;
                    config.configInfo.prefabName = tmp_prefabName;
                    ConfigSave();
                }
            }
            catch (System.Exception)
            {
                System.Type type = typeof(BoneInfo);
                if (config == null || ReorderListNullCheck(type) || reorderLists[type].mainReorderList == null)
                {
                    Init();
                }

                reorderLists[type].mainReorderList.list = config.configInfo.boneInfoList;
            }
        }

        private ReorderInfo ReorderInfoCheck(System.Type type)
        {
            ReorderInfo reorderInfo;

            try
            {
                reorderInfo = reorderLists[type];
                if (reorderInfo == null)
                {
                    reorderInfo = new ReorderInfo();
                }
            }
            catch (KeyNotFoundException)
            {
                reorderLists.Add(type, new ReorderInfo());
                reorderInfo = reorderLists[type];
            }

            return reorderInfo;
        }

        private bool ReorderListNullCheck(System.Type type)
        {
            return reorderLists == null || !reorderLists.ContainsKey(type) || reorderLists[type] == null;
        }

        private void DrawReorderList(string title, System.Type type, ClipFold clipFold)
        {
            clipFold.mainFoldFlag = FigureAvatarUtil.Styles.Foldout(title, clipFold.mainFoldFlag);
            if (clipFold.mainFoldFlag)
            {
                EditorGUILayout.BeginHorizontal();
                {
                    GUILayout.Space(10);
                    EditorGUILayout.BeginVertical();
                    {
                        if (!ReorderListNullCheck(type) && reorderLists[type].mainReorderList != null)
                        {
                            EditorGUILayout.Space();
                            reorderLists[type].mainReorderList.DoLayoutList();
                        }

                        EditorGUILayout.Space();
                        clipFold.clipFoldFlag = FigureAvatarUtil.Styles.Foldout("Clips", clipFold.clipFoldFlag);
                        if (clipFold.clipFoldFlag)
                        {
                            if (!ReorderListNullCheck(type) && reorderLists[type].clipReorderList != null && reorderLists[type].mainReorderList.count > 0)
                            {
                                EditorGUILayout.Space();
                                reorderLists[type].clipReorderList.DoLayoutList();
                            }
                        }
                    }
                    EditorGUILayout.EndVertical();
                }
                EditorGUILayout.EndHorizontal();
            }
        }

        private void ConfigSave()
        {
            if (config != null)
            {
                ReorderInfo reorderInfo = ReorderInfoCheck(typeof(BoneInfo));
                if (reorderInfo.mainReorderList != null)
                {
                    config.configInfo.boneInfoList = reorderInfo.mainReorderList.list as List<BoneInfo>;
                    if (config.configInfo.boneInfoList != null && reorderInfo.mainReorderList.index < config.configInfo.boneInfoList.Count && reorderInfo.mainReorderList.index >= 0)
                    {
                        if (reorderLists.Count > 0)
                        {
                            config.configInfo.boneInfoList[reorderInfo.mainReorderList.index].clipList = reorderInfo.clipReorderList.list as List<ClipInfo>;
                        }
                    }
                }
                EditorUtility.SetDirty(config);
            }
        }

        private void AvatarChanged()
        {
            if (config.avatarObj == null)
            {
                return;
            }

            Animator animator = AnimatorCheck(config.avatarObj);
            if (animator == null)
            {
                return;
            }

            bool avatarObjActiveFlag = config.avatarObj.activeSelf;
            if (!avatarObjActiveFlag)
            {
                config.avatarObj.SetActive(true);
            }

            /*FigureAvatarInfo avatarInfo = config.avatarObj.GetComponent<FigureAvatarInfo>();
            if (avatarInfo != null)
            {
                editMode = true;
                config.boneInfoList = avatarInfo.boneInfoList;
                config.idleInfo = avatarInfo.idleInfo;
                config.approachInfo = avatarInfo.approachInfo;
                config.configInfo.eventObjs = avatarInfo.eventObjs;
            }*/

            config.configInfo.prefabName = config.avatarObj.name + (config.avatarObj.name.EndsWith("_FigureAvatar") ? "" : "_FigureAvatar");

            activeBoneList.Clear();
            bool flag = false;
            foreach (HumanBodyBones bone in System.Enum.GetValues(typeof(HumanBodyBones)))
            {
                Transform boneTransform = animator.GetBoneTransform(bone);
                if (boneTransform != null)
                {
                    flag = true;
                    activeBoneList.Add(bone);
                }
            }
            if (!flag)
            {
                FigureAvatarUtil.ErrorMessage("アバターがHumanoid規格になっていない・Armatureが非アクティブになっています。");
                Destructor();
                return;
            }

            ReorderInfo reorderInfo = ReorderInfoCheck(typeof(BoneInfo));
            foreach (BoneInfo boneInfo in reorderInfo.mainReorderList.list)
            {
                if (!(reorderInfo.mainReorderList.list as List<BoneInfo>).Select(b => b.bone).Contains(boneInfo.bone))
                {
                    boneInfo.bone = HumanBodyBones.Hips;
                }
            }

            if (reorderInfo.mainReorderList.list.Count == 0)
            {
                foreach (HumanBodyBones bone in new HumanBodyBones[] { HumanBodyBones.Head, HumanBodyBones.Spine, HumanBodyBones.LeftHand, HumanBodyBones.RightHand })
                {
                    BoneInfo boneInfo = new BoneInfo();
                    boneInfo.bone = bone;
                    boneInfo.colSize = 0.1f;
                    reorderInfo.mainReorderList.list.Add(boneInfo);
                }

                reorderInfo.mainReorderList.index = -1;
                reorderInfo.clipReorderList = null;
            }

            if (reorderInfo.mainReorderList.count > 0 && reorderInfo.mainReorderList.index == -1)
            {
                reorderInfo.mainReorderList.index = 0;
                reorderInfo.mainReorderList.onSelectCallback.Invoke(reorderInfo.mainReorderList);
            }

            config.avatarObj.SetActive(avatarObjActiveFlag);
        }

        private int GetHumanBoneToListIndex(HumanBodyBones bone, List<HumanBodyBones> boneList)
        {
            for (int i = 0; i < boneList.Count; i++)
            {
                if (bone == boneList[i])
                {
                    return i;
                }
            }

            return 0;
        }

        private Animator AnimatorCheck(GameObject obj)
        {
            if (obj == null)
            {
                return null;
            }

            Animator animator = obj.GetOrAddComponent<Animator>();
            if (animator == null)
            {
                FigureAvatarUtil.ErrorMessage("アバターにAnimatorをアタッチしてください。");
                Destructor();
                return null;
            }

            if (animator.avatar == null)
            {
                FigureAvatarUtil.ErrorMessage("AnimatorにAvatarがセットされていません。");
                Destructor();
                return null;
            }

            return animator;
        }

        private void GenerateAvatar()
        {
            if (config.avatarObj == null)
            {
                FigureAvatarUtil.ErrorMessage("アバターをセットしてください。");
                Destructor();
                return;
            }

            StandardAssetLoad();
            if (!editMode)
            {
                config.configInfo.eventObjs.prefabObj = Instantiate(config.avatarObj);
                config.avatarObj.SetActive(false);
            }
            else
            {
                config.configInfo.eventObjs.prefabObj = config.avatarObj;
            }

            if (config.configInfo.eventObjs.prefabObj == null)
            {
                FigureAvatarUtil.ErrorMessage("プレハブの生成に失敗しました。");
                Destructor();
                return;
            }

            if (!config.configInfo.eventObjs.prefabObj.activeSelf)
            {
                config.configInfo.eventObjs.prefabObj.SetActive(true);
            }

            config.configInfo.eventObjs.prefabObj.name = config.configInfo.prefabName;

            VRC_AvatarDescriptor avatarDescriptor = config.configInfo.eventObjs.prefabObj.GetComponent<VRC_AvatarDescriptor>();
            if (avatarDescriptor != null)
            {
                DestroyImmediate(avatarDescriptor);
            }
            VRC.Core.PipelineManager pipelineManager = config.configInfo.eventObjs.prefabObj.GetComponent<VRC.Core.PipelineManager>();
            if (pipelineManager != null)
            {
                DestroyImmediate(pipelineManager);
            }

            /*FigureAvatarInfo avatarInfo = config.configInfo.eventObjs.prefabObj.GetOrAddComponent<FigureAvatarInfo>();
            if (avatarInfo == null)
            {
                FigureAvatarUtil.ErrorMessage("プレハブの生成に失敗しました。");
                Destructor();
                return;
            }*/

            Animator animator = AnimatorCheck(config.configInfo.eventObjs.prefabObj);
            if (animator == null)
            {
                return;
            }

            string p = AssetDatabase.GetAssetPath(config.configInfo.saveFolder) + "/" + config.configInfo.prefabName + ".prefab";
            string outPath = editMode ? p : AssetDatabase.GenerateUniqueAssetPath(p);
            string outAnimatorPath = FigureAvatarUtil.controllersFolderPath + "/" + config.configInfo.prefabName + ".controller";
            UnityEditor.Animations.AnimatorController animatorController = editMode ? AssetDatabase.LoadAssetAtPath<UnityEditor.Animations.AnimatorController>(outAnimatorPath) : UnityEditor.Animations.AnimatorController.CreateAnimatorControllerAtPath(outAnimatorPath);

            if (!editMode && System.IO.File.Exists(p))
            {
                if (FigureAvatarUtil.MessageBox("Warning!", "同名のプレハブが存在しますが上書きしますか？", FigureAvatarUtil.MessageType.YesNo))
                {
                    outPath = p;
                    animatorController = AssetDatabase.LoadAssetAtPath<UnityEditor.Animations.AnimatorController>(outAnimatorPath);
                }
            }

            UnityEditor.Animations.AnimatorStateMachine stateMachine = animatorController.layers[0].stateMachine;

            Transform headTransform = animator.GetBoneTransform(HumanBodyBones.Head);
            Transform neckTransform = animator.GetBoneTransform(HumanBodyBones.Neck);

            if (!editMode)
            {
                config.configInfo.eventObjs.audioSourceObj = new GameObject("AudioSource");
                config.configInfo.eventObjs.eventsObj = new GameObject("Events");
                config.configInfo.eventObjs.lookSwitcherObj = new GameObject("LookSwitcher");
                config.configInfo.eventObjs.trueObj = new GameObject("True");
                config.configInfo.eventObjs.falseObj = new GameObject("False");
                config.configInfo.eventObjs.textDebrisObj = new GameObject("TextDebris");
                config.configInfo.eventObjs.textObj = new GameObject("Text");
                config.configInfo.eventObjs.idlesObj = new GameObject("Idles");
                config.configInfo.eventObjs.buttonsObj = new GameObject("Buttons");
                config.configInfo.eventObjs.animClipsObj = new GameObject("AnimClips");
                config.configInfo.eventObjs.audioClipsObj = new GameObject("AudioClips");
            }

            try
            {
                config.configInfo.eventObjs.audioSourceObj.transform.SetParent(headTransform);
                TransformReset(config.configInfo.eventObjs.audioSourceObj.transform);
                config.configInfo.eventObjs.eventsObj.transform.SetParent(config.configInfo.eventObjs.prefabObj.transform);
                TransformReset(config.configInfo.eventObjs.eventsObj.transform);
                config.configInfo.eventObjs.lookSwitcherObj.transform.SetParent(config.configInfo.eventObjs.eventsObj.transform);
                TransformReset(config.configInfo.eventObjs.lookSwitcherObj.transform);
                config.configInfo.eventObjs.trueObj.transform.SetParent(config.configInfo.eventObjs.lookSwitcherObj.transform);
                TransformReset(config.configInfo.eventObjs.trueObj.transform);
                config.configInfo.eventObjs.falseObj.transform.SetParent(config.configInfo.eventObjs.lookSwitcherObj.transform);
                TransformReset(config.configInfo.eventObjs.falseObj.transform);
                config.configInfo.eventObjs.textDebrisObj.transform.SetParent(config.configInfo.eventObjs.eventsObj.transform);
                TransformReset(config.configInfo.eventObjs.textDebrisObj.transform);
                config.configInfo.eventObjs.textObj.transform.SetParent(config.configInfo.eventObjs.textDebrisObj.transform);
                TransformReset(config.configInfo.eventObjs.textObj.transform);
                config.configInfo.eventObjs.idlesObj.transform.SetParent(config.configInfo.eventObjs.eventsObj.transform);
                TransformReset(config.configInfo.eventObjs.idlesObj.transform);
                config.configInfo.eventObjs.buttonsObj.transform.SetParent(config.configInfo.eventObjs.eventsObj.transform);
                TransformReset(config.configInfo.eventObjs.buttonsObj.transform);
                config.configInfo.eventObjs.animClipsObj.transform.SetParent(config.configInfo.eventObjs.eventsObj.transform);
                TransformReset(config.configInfo.eventObjs.animClipsObj.transform);
                config.configInfo.eventObjs.audioClipsObj.transform.SetParent(config.configInfo.eventObjs.eventsObj.transform);
                TransformReset(config.configInfo.eventObjs.audioClipsObj.transform);
            }
            catch (System.NullReferenceException)
            {
                FigureAvatarUtil.ErrorMessage("プレハブの編集に失敗しました。");
                Destructor();
                return;
            }

            if (editMode)
            {
                foreach (Transform transform in config.configInfo.eventObjs.buttonsObj.transform.Cast<Transform>().Concat(config.configInfo.eventObjs.animClipsObj.transform.Cast<Transform>()).Concat(config.configInfo.eventObjs.audioClipsObj.transform.Cast<Transform>()))
                {
                    DestroyImmediate(transform.gameObject, true);
                }
            }

            AudioSource audioSource = config.configInfo.eventObjs.audioSourceObj.GetOrAddComponent<AudioSource>();

            AddPressButton(config.configInfo.eventObjs.trueObj, new UnityEventUtil.UnityEventCalls.Fields[] { new UnityEventUtil.UnityEventCalls.Fields(UnityEngine.Events.PersistentListenerMode.String, "BroadcastMessage", config.configInfo.eventObjs.buttonsObj, UnityEngine.Events.UnityEventCallState.RuntimeOnly, new UnityEventUtil.UnityEventCalls.Arguments(null, "", 0, 0, "SendOnValueChanged")) }, FigureAvatarUtil.pressManualAnimatorPath);
            AddPressButton(config.configInfo.eventObjs.falseObj, new UnityEventUtil.UnityEventCalls.Fields[] { new UnityEventUtil.UnityEventCalls.Fields(UnityEngine.Events.PersistentListenerMode.String, "BroadcastMessage", config.configInfo.eventObjs.buttonsObj, UnityEngine.Events.UnityEventCallState.RuntimeOnly, new UnityEventUtil.UnityEventCalls.Arguments(null, "", 0, 0, "SendOnSubmit")) }, FigureAvatarUtil.pressManualAnimatorPath);
            RectTransform textRectTransform = config.configInfo.eventObjs.textObj.GetOrAddComponent<RectTransform>();
            textRectTransform.sizeDelta = Vector2.zero;
            Text text = config.configInfo.eventObjs.textObj.GetOrAddComponent<Text>();
            text.enabled = false;

            if (config.typesFlag)
            {
                if (!editMode)
                {
                    config.configInfo.eventObjs.playerTrackingObj = new GameObject("PlayerTracking");
                }

                try
                {
                    config.configInfo.eventObjs.playerTrackingObj.transform.SetParent(config.configInfo.eventObjs.eventsObj.transform);
                    RectTransform playerTrackingRectTransform = config.configInfo.eventObjs.playerTrackingObj.GetOrAddComponent<RectTransform>();
                    Canvas canvas = config.configInfo.eventObjs.playerTrackingObj.GetOrAddComponent<Canvas>();
                    canvas.renderMode = RenderMode.ScreenSpaceCamera;
                    canvas.planeDistance = 0;
                    BoxCollider collider = config.configInfo.eventObjs.playerTrackingObj.GetOrAddComponent<BoxCollider>();
                    collider.isTrigger = true;
                    collider.enabled = false;
                    config.configInfo.eventObjs.playerTrackingObj.GetOrAddComponent<VRC_UiShape>();
                    GameObject lookatObj = new GameObject("Lookat");
                    lookatObj.transform.SetParent(headTransform);
                    TransformReset(lookatObj.transform);
                    Component lookat = lookatObj.AddComponent(types.lookatTargetType);
                    SerializedObject lookatSerializedObject = new SerializedObject(lookat);
                    lookatSerializedObject.FindProperty("m_RotationRange").vector2Value = new Vector2(80, 80);
                    lookatSerializedObject.FindProperty("m_FollowSpeed").floatValue = 0.5f;
                    lookatSerializedObject.FindProperty("m_UpdateType").enumValueIndex = 1;
                    lookatSerializedObject.FindProperty("m_AutoTargetPlayer").boolValue = false;
                    lookatSerializedObject.FindProperty("m_Target").objectReferenceValue = playerTrackingRectTransform;
                    lookatSerializedObject.ApplyModifiedProperties();
                    GameObject neckLookatObj = new GameObject("Lookat");
                    neckLookatObj.transform.SetParent(neckTransform);
                    TransformReset(neckLookatObj.transform);
                    Component neckLookat = neckLookatObj.AddComponent(types.lookatTargetType);
                    SerializedObject neckLookatSerializedObject = new SerializedObject(neckLookat);
                    neckLookatSerializedObject.FindProperty("m_RotationRange").vector2Value = new Vector2(15, 30);
                    neckLookatSerializedObject.FindProperty("m_FollowSpeed").floatValue = 0.5f;
                    neckLookatSerializedObject.FindProperty("m_UpdateType").enumValueIndex = 1;
                    neckLookatSerializedObject.FindProperty("m_AutoTargetPlayer").boolValue = false;
                    neckLookatSerializedObject.FindProperty("m_Target").objectReferenceValue = playerTrackingRectTransform;
                    neckLookatSerializedObject.ApplyModifiedProperties();
                    Camera camera = headTransform.gameObject.GetOrAddComponent<Camera>();
                    camera.clearFlags = CameraClearFlags.Nothing;
                    camera.cullingMask = 0;
                    camera.orthographic = true;
                    camera.orthographicSize = 0.001f;
                    camera.allowMSAA = false;
                    camera.allowHDR = false;
                    camera.nearClipPlane = 0;
                    camera.farClipPlane = 0.01f;
                    camera.enabled = false;
                    Component autoCam = headTransform.gameObject.AddComponent(types.autoCamType);
                    SerializedObject autoCamSerializedObject = new SerializedObject(autoCam);
                    autoCamSerializedObject.FindProperty("m_UpdateType").enumValueIndex = 1;
                    autoCamSerializedObject.FindProperty("m_AutoTargetPlayer").boolValue = false;
                    autoCamSerializedObject.FindProperty("m_Target").objectReferenceValue = lookatObj.transform;
                    autoCamSerializedObject.FindProperty("m_MoveSpeed").floatValue = 0;
                    autoCamSerializedObject.FindProperty("m_TurnSpeed").floatValue = 0;
                    autoCamSerializedObject.FindProperty("m_RollSpeed").floatValue = 0;
                    autoCamSerializedObject.FindProperty("m_SpinTurnLimit").floatValue = 0;
                    autoCamSerializedObject.FindProperty("m_TargetVelocityLowerLimit").floatValue = 0;
                    autoCamSerializedObject.FindProperty("m_SmoothTurnTime").floatValue = 0;
                    autoCamSerializedObject.ApplyModifiedProperties();
                    Camera neckCamera = neckTransform.gameObject.GetOrAddComponent<Camera>();
                    neckCamera.clearFlags = CameraClearFlags.Nothing;
                    neckCamera.cullingMask = 0;
                    neckCamera.orthographic = true;
                    neckCamera.orthographicSize = 0.001f;
                    neckCamera.allowMSAA = false;
                    neckCamera.allowHDR = false;
                    neckCamera.nearClipPlane = 0;
                    neckCamera.farClipPlane = 0.01f;
                    neckCamera.enabled = false;
                    Component neckAutoCam = neckTransform.gameObject.AddComponent(types.autoCamType);
                    SerializedObject neckAutoCamSerializedObject = new SerializedObject(neckAutoCam);
                    neckAutoCamSerializedObject.FindProperty("m_UpdateType").enumValueIndex = 1;
                    neckAutoCamSerializedObject.FindProperty("m_AutoTargetPlayer").boolValue = false;
                    neckAutoCamSerializedObject.FindProperty("m_Target").objectReferenceValue = neckLookatObj.transform;
                    neckAutoCamSerializedObject.FindProperty("m_MoveSpeed").floatValue = 0;
                    neckAutoCamSerializedObject.FindProperty("m_TurnSpeed").floatValue = 0;
                    neckAutoCamSerializedObject.FindProperty("m_RollSpeed").floatValue = 0;
                    neckAutoCamSerializedObject.FindProperty("m_SpinTurnLimit").floatValue = 0;
                    neckAutoCamSerializedObject.FindProperty("m_TargetVelocityLowerLimit").floatValue = 0;
                    neckAutoCamSerializedObject.FindProperty("m_SmoothTurnTime").floatValue = 0;
                    neckAutoCamSerializedObject.ApplyModifiedProperties();
                    Animator lookatManagerAnimator = headTransform.gameObject.GetOrAddComponent<Animator>();
                    Animator neckLookatManagerAnimator = neckTransform.gameObject.GetOrAddComponent<Animator>();
                    RuntimeAnimatorController lookatManagerAnimatorController = AssetDatabase.LoadAssetAtPath<RuntimeAnimatorController>(FigureAvatarUtil.lookatManagerAnimatorPath);
                    lookatManagerAnimator.runtimeAnimatorController = lookatManagerAnimatorController;
                    lookatManagerAnimator.cullingMode = AnimatorCullingMode.AlwaysAnimate;
                    neckLookatManagerAnimator.runtimeAnimatorController = lookatManagerAnimatorController;
                    neckLookatManagerAnimator.cullingMode = AnimatorCullingMode.AlwaysAnimate;
                }
                catch (System.NullReferenceException)
                {
                    FigureAvatarUtil.ErrorMessage("プレハブの編集に失敗しました。");
                    Destructor();
                    return;
                }
            }

            // Bone
            foreach (BoneInfo boneInfo in ReorderInfoCheck(typeof(BoneInfo)).mainReorderList.list)
            {
                Transform bone = animator.GetBoneTransform(boneInfo.bone);
                if (bone == null)
                {
                    FigureAvatarUtil.ErrorMessage("アバターがHumanoid規格になっていません。");
                    Destructor();
                    return;
                }

                if (!editMode)
                {
                    config.configInfo.eventObjs.boneColliderObjs[(int) boneInfo.bone] = new GameObject("Collider");
                }

                try
                {
                    if (config.configInfo.eventObjs.boneColliderObjs[(int) boneInfo.bone] == null)
                    {
                        throw new System.NullReferenceException();
                    }

                    VRC_Trigger trigger = config.configInfo.eventObjs.boneColliderObjs[(int) boneInfo.bone].GetOrAddComponent<VRC_Trigger>();
                    try
                    {
                        trigger.GetType().GetField("UsesAdvancedOptions").SetValue(trigger, true);
                    }
                    catch { }
                    trigger.proximity = 0.184f;
                    SphereCollider collider = config.configInfo.eventObjs.boneColliderObjs[(int) boneInfo.bone].GetOrAddComponent<SphereCollider>();
                    Transform colliderTransform = config.configInfo.eventObjs.boneColliderObjs[(int) boneInfo.bone].transform;
                    if (editMode)
                    {
                        colliderTransform.SetParent(null);
                    }
                    colliderTransform.localRotation = Quaternion.Euler(0, 0, 0);
                    colliderTransform.localScale = Vector3.one;
                    colliderTransform.SetParent(bone);
                    colliderTransform.localPosition = Vector3.zero;
                    colliderTransform.SetParent(null);
                    colliderTransform.localPosition = colliderTransform.localPosition + boneInfo.offset;
                    colliderTransform.SetParent(bone);
                    colliderTransform.localRotation = Quaternion.Euler(0, 0, 0);
                    collider.radius = boneInfo.colSize;
                    collider.isTrigger = true;
                    trigger.interactText = "Touch";
                    VRC_Trigger.TriggerEvent triggerEvent = new VRC_Trigger.TriggerEvent();
                    triggerEvent.TriggerType = VRC_Trigger.TriggerType.OnInteract;
                    triggerEvent.BroadcastType = VRC_EventHandler.VrcBroadcastType.Local;
                    {
                        VRC_EventHandler.VrcEvent vrcEvent = new VRC_EventHandler.VrcEvent();
                        vrcEvent.EventType = VRC_EventHandler.VrcEventType.ActivateCustomTrigger;
                        vrcEvent.ParameterString = "Random";
                        vrcEvent.ParameterObjects = new GameObject[] { config.configInfo.eventObjs.boneColliderObjs[(int)boneInfo.bone] };
                        triggerEvent.Events.Add(vrcEvent);
                    }
                    trigger.Triggers.Add(triggerEvent);
                    triggerEvent = new VRC_Trigger.TriggerEvent();
                    triggerEvent.TriggerType = VRC_Trigger.TriggerType.Custom;
                    triggerEvent.Name = "Random";
                    triggerEvent.BroadcastType = VRC_EventHandler.VrcBroadcastType.MasterUnbuffered;
                    triggerEvent = SetProbabilitie(triggerEvent, boneInfo.clipList.Select(b => b.random).ToArray(), boneInfo.autoRandom);

                    foreach (ClipInfo clipInfo in boneInfo.clipList)
                    {
                        string animTriggerName = clipInfo.animClip == null ? "" : boneInfo.bone.ToString() + "_" + clipInfo.animClip.name;
                        SetClip(animator, animatorController, triggerEvent, config.configInfo.eventObjs, audioSource, clipInfo, animTriggerName);
                    }
                    trigger.Triggers.Add(triggerEvent);
                }
                catch (System.NullReferenceException)
                {
                    FigureAvatarUtil.ErrorMessage("プレハブの編集に失敗しました。");
                    Destructor();
                    return;
                }
            }

            // Approach
            {
                ApproachInfo approachInfo = config.configInfo.approachInfo;
                if (!editMode)
                {
                    config.configInfo.eventObjs.approachColliderObj = new GameObject("Collider");
                }

                try
                {
                    VRC_Trigger trigger = config.configInfo.eventObjs.approachColliderObj.GetOrAddComponent<VRC_Trigger>();
                    try
                    {
                        trigger.GetType().GetField("UsesAdvancedOptions").SetValue(trigger, true);
                    }
                    catch { }
                    config.configInfo.eventObjs.approachColliderObj.layer = LayerMask.NameToLayer("MirrorReflection");
                    MeshCollider collider = config.configInfo.eventObjs.approachColliderObj.GetOrAddComponent<MeshCollider>();
                    Mesh mesh = null;
                    foreach (Object obj in AssetDatabase.LoadAllAssetsAtPath("Library/unity default resources"))
                    {
                        if (obj != null && obj.GetType() == typeof(Mesh))
                        {
                            Mesh _mesh = obj as Mesh;
                            if (_mesh.name == "Cylinder" && _mesh.vertices.Length == 88)
                            {
                                mesh = _mesh;
                            }
                        }
                    }
                    collider.sharedMesh = mesh;
                    config.configInfo.eventObjs.approachColliderObj.transform.localScale = new Vector3(approachInfo.radius * 2, 0.001f, approachInfo.radius * 2);
                    collider.convex = true;
                    collider.isTrigger = true;
                    Transform colliderTransform = config.configInfo.eventObjs.approachColliderObj.transform;
                    colliderTransform.SetParent(null);
                    colliderTransform.localPosition = new Vector3(0, 0.5f, 0);
                    colliderTransform.localRotation = Quaternion.Euler(0, 0, 0);
                    colliderTransform.SetParent(config.configInfo.eventObjs.prefabObj.transform);
                    colliderTransform.localPosition = new Vector3(0, colliderTransform.localPosition.y, 0);
                    VRC_Trigger.TriggerEvent triggerEvent = new VRC_Trigger.TriggerEvent();
                    triggerEvent.TriggerType = VRC_Trigger.TriggerType.OnEnterTrigger;
                    triggerEvent.BroadcastType = VRC_EventHandler.VrcBroadcastType.Local;
                    triggerEvent.Layers = 1 << LayerMask.NameToLayer("PlayerLocal");
                    {
                        VRC_EventHandler.VrcEvent vrcEvent = new VRC_EventHandler.VrcEvent();
                        vrcEvent.EventType = VRC_EventHandler.VrcEventType.ActivateCustomTrigger;
                        vrcEvent.ParameterString = "Random";
                        vrcEvent.ParameterObjects = new GameObject[] { config.configInfo.eventObjs.approachColliderObj };
                        triggerEvent.Events.Add(vrcEvent);
                    }
                    trigger.Triggers.Add(triggerEvent);
                    triggerEvent = new VRC_Trigger.TriggerEvent();
                    triggerEvent.TriggerType = VRC_Trigger.TriggerType.Custom;
                    triggerEvent.Name = "Random";
                    triggerEvent.BroadcastType = VRC_EventHandler.VrcBroadcastType.MasterUnbuffered;
                    triggerEvent = SetProbabilitie(triggerEvent, approachInfo.clipList.Select(b => b.random).ToArray(), approachInfo.autoRandom);
                    foreach (ClipInfo clipInfo in approachInfo.clipList)
                    {
                        string animTriggerName = clipInfo.animClip == null ? "" : "Approach_" + clipInfo.animClip.name;
                        SetClip(animator, animatorController, triggerEvent, config.configInfo.eventObjs, audioSource, clipInfo, animTriggerName);
                    }
                    trigger.Triggers.Add(triggerEvent);

                    if (types.lookatTargetType != null)
                    {
                        triggerEvent = new VRC_Trigger.TriggerEvent();
                        triggerEvent.TriggerType = VRC_Trigger.TriggerType.OnEnterTrigger;
                        triggerEvent.BroadcastType = VRC_EventHandler.VrcBroadcastType.Local;
                        triggerEvent.Layers = 1 << LayerMask.NameToLayer("PlayerLocal");
                        VRC_EventHandler.VrcEvent vrcEvent = new VRC_EventHandler.VrcEvent();
                        vrcEvent.EventType = VRC_EventHandler.VrcEventType.AnimationBool;
                        vrcEvent.ParameterBoolOp = VRC_EventHandler.VrcBooleanOp.True;
                        vrcEvent.ParameterString = "Lookat";
                        vrcEvent.ParameterObjects = new GameObject[] { headTransform.gameObject, neckTransform.gameObject };
                        triggerEvent.Events.Add(vrcEvent);
                        vrcEvent = new VRC_EventHandler.VrcEvent();
                        vrcEvent.EventType = VRC_EventHandler.VrcEventType.AnimationTrigger;
                        vrcEvent.ParameterString = "Trigger";
                        vrcEvent.ParameterObjects = new GameObject[] { config.configInfo.eventObjs.trueObj };
                        triggerEvent.Events.Add(vrcEvent);
                        trigger.Triggers.Add(triggerEvent);

                        triggerEvent = new VRC_Trigger.TriggerEvent();
                        triggerEvent.TriggerType = VRC_Trigger.TriggerType.OnExitTrigger;
                        triggerEvent.BroadcastType = VRC_EventHandler.VrcBroadcastType.Local;
                        triggerEvent.Layers = 1 << LayerMask.NameToLayer("PlayerLocal");
                        vrcEvent = new VRC_EventHandler.VrcEvent();
                        vrcEvent.EventType = VRC_EventHandler.VrcEventType.AnimationBool;
                        vrcEvent.ParameterBoolOp = VRC_EventHandler.VrcBooleanOp.False;
                        vrcEvent.ParameterString = "Lookat";
                        vrcEvent.ParameterObjects = new GameObject[] { headTransform.gameObject, neckTransform.gameObject };
                        triggerEvent.Events.Add(vrcEvent);
                        vrcEvent = new VRC_EventHandler.VrcEvent();
                        vrcEvent.EventType = VRC_EventHandler.VrcEventType.AnimationTrigger;
                        vrcEvent.ParameterString = "Trigger";
                        vrcEvent.ParameterObjects = new GameObject[] { config.configInfo.eventObjs.falseObj };
                        triggerEvent.Events.Add(vrcEvent);
                        trigger.Triggers.Add(triggerEvent);
                    }
                }
                catch (System.NullReferenceException)
                {
                    FigureAvatarUtil.ErrorMessage("プレハブの編集に失敗しました。");
                    Destructor();
                    return;
                }
            }

            // Idle
            UnityEditor.Animations.AnimatorState state = stateMachine.AddState("Idle", new Vector3(-200, 200, 0));
            state.motion = config.configInfo.idleInfo.defaultAnimClip;
            stateMachine.defaultState = state;
            UnityEditor.Animations.AnimatorState state2 = stateMachine.AddState("Idle2", new Vector3(50, 200, 0));
            state2.motion = config.configInfo.idleInfo.defaultAnimClip;
            UnityEditor.Animations.AnimatorStateTransition transition = state.AddTransition(state2);
            transition.hasExitTime = true;
            transition.exitTime = 1;
            transition.duration = 0.2f;
            transition.canTransitionToSelf = false;
            UnityEditor.Animations.AnimatorStateTransition transition2 = state2.AddTransition(state);
            transition2.hasExitTime = true;
            transition2.exitTime = 1;
            transition2.duration = 0.2f;
            transition2.canTransitionToSelf = false;
            {
                IdleInfo idleInfo = config.configInfo.idleInfo;
                VRC_Trigger trigger = config.configInfo.eventObjs.idlesObj.GetOrAddComponent<VRC_Trigger>();
                try
                {
                    trigger.GetType().GetField("UsesAdvancedOptions").SetValue(trigger, true);
                }
                catch { }
                VRC_Trigger.TriggerEvent triggerEvent = new VRC_Trigger.TriggerEvent();
                triggerEvent.TriggerType = VRC_Trigger.TriggerType.Custom;
                triggerEvent.BroadcastType = VRC_EventHandler.VrcBroadcastType.MasterUnbuffered;
                triggerEvent.Name = "Idle";
                if (idleInfo.autoRandom)
                {
                    triggerEvent = SetProbabilitie(triggerEvent, idleInfo.clipList.Select(b => b.random).ToArray(), idleInfo.autoRandom);
                }
                foreach (ClipInfo clipInfo in idleInfo.clipList)
                {
                    string animTriggerName = clipInfo.animClip == null ? "" : "Idle_" + clipInfo.animClip.name;
                    SetClip(animator, animatorController, triggerEvent, config.configInfo.eventObjs, audioSource, clipInfo, animTriggerName, true, state, state2);
                }
                trigger.Triggers.Add(triggerEvent);
                triggerEvent = new VRC_Trigger.TriggerEvent();
                triggerEvent.TriggerType = VRC_Trigger.TriggerType.Custom;
                triggerEvent.BroadcastType = VRC_EventHandler.VrcBroadcastType.Local;
                triggerEvent.Name = "Random";
                triggerEvent = SetProbabilitie(triggerEvent, new float[] { idleInfo.random }, true);
                VRC_EventHandler.VrcEvent vrcEvent = new VRC_EventHandler.VrcEvent();
                vrcEvent.EventType = VRC_EventHandler.VrcEventType.ActivateCustomTrigger;
                vrcEvent.ParameterString = "Idle";
                vrcEvent.ParameterObjects = new GameObject[] { config.configInfo.eventObjs.idlesObj };
                triggerEvent.Events.Add(vrcEvent);
                trigger.Triggers.Add(triggerEvent);
                triggerEvent = new VRC_Trigger.TriggerEvent();
                triggerEvent.TriggerType = VRC_Trigger.TriggerType.Custom;
                triggerEvent.BroadcastType = VRC_EventHandler.VrcBroadcastType.Local;
                triggerEvent.AfterSeconds = idleInfo.interval;
                triggerEvent.Name = "Execute";
                vrcEvent = new VRC_EventHandler.VrcEvent();
                vrcEvent.EventType = VRC_EventHandler.VrcEventType.ActivateCustomTrigger;
                vrcEvent.ParameterString = "Random";
                vrcEvent.ParameterObjects = new GameObject[] { config.configInfo.eventObjs.idlesObj };
                triggerEvent.Events.Add(vrcEvent);

                if (!editMode)
                {
                    config.configInfo.eventObjs.setupObj = new GameObject("Setup");
                }

                try
                {
                    config.configInfo.eventObjs.setupObj.transform.SetParent(config.configInfo.eventObjs.idlesObj.transform);
                    TransformReset(config.configInfo.eventObjs.setupObj.transform);
                    Animator pressAnimator = AddPressAnimator(config.configInfo.eventObjs.setupObj, FigureAvatarUtil.pressAnimatorPath);
                    pressAnimator.enabled = false;
                    Button button = config.configInfo.eventObjs.setupObj.GetOrAddComponent<Button>();
                    SetButtonEvents(button, new UnityEventUtil.UnityEventCalls.Fields[]
                    {
                        new UnityEventUtil.UnityEventCalls.Fields(UnityEngine.Events.PersistentListenerMode.Bool, "SetActive", config.configInfo.eventObjs.setupObj, UnityEngine.Events.UnityEventCallState.RuntimeOnly, new UnityEventUtil.UnityEventCalls.Arguments(null, "", 0, 0, "", false)),
                        new UnityEventUtil.UnityEventCalls.Fields(UnityEngine.Events.PersistentListenerMode.String, "ExecuteCustomTrigger", trigger, UnityEngine.Events.UnityEventCallState.RuntimeOnly, new UnityEventUtil.UnityEventCalls.Arguments(null, "", 0, 0, "Execute"))
                    });
                    vrcEvent = new VRC_EventHandler.VrcEvent();
                    vrcEvent.EventType = VRC_EventHandler.VrcEventType.SetGameObjectActive;
                    vrcEvent.ParameterBoolOp = VRC_EventHandler.VrcBooleanOp.True;
                    vrcEvent.ParameterObjects = new GameObject[] { config.configInfo.eventObjs.setupObj };
                    triggerEvent.Events.Add(vrcEvent);
                    trigger.Triggers.Add(triggerEvent);

                    VRC_AudioBank audioBank = config.configInfo.eventObjs.idlesObj.GetOrAddComponent<VRC_AudioBank>();

                    triggerEvent = new VRC_Trigger.TriggerEvent();
                    triggerEvent.TriggerType = VRC_Trigger.TriggerType.Custom;
                    triggerEvent.BroadcastType = VRC_EventHandler.VrcBroadcastType.Local;
                    triggerEvent.Name = "MasterLocal";
                    vrcEvent = new VRC_EventHandler.VrcEvent();
                    vrcEvent.EventType = VRC_EventHandler.VrcEventType.SetComponentActive;
                    vrcEvent.ParameterString = "UnityEngine.Animator";
                    vrcEvent.ParameterBoolOp = VRC_EventHandler.VrcBooleanOp.True;
                    vrcEvent.ParameterObjects = new GameObject[] { config.configInfo.eventObjs.setupObj };
                    triggerEvent.Events.Add(vrcEvent);
                    trigger.Triggers.Add(triggerEvent);

                    triggerEvent = new VRC_Trigger.TriggerEvent();
                    triggerEvent.TriggerType = VRC_Trigger.TriggerType.OnNetworkReady;
                    triggerEvent.BroadcastType = VRC_EventHandler.VrcBroadcastType.MasterUnbuffered;
                    vrcEvent = new VRC_EventHandler.VrcEvent();
                    vrcEvent.EventType = VRC_EventHandler.VrcEventType.SendRPC;
                    vrcEvent.ParameterString = "Play";
                    vrcEvent.ParameterInt = 6;
                    vrcEvent.ParameterBytes = VRC_Serialization.ParameterEncoder(new object[] { 0 });
                    vrcEvent.ParameterObjects = new GameObject[] { config.configInfo.eventObjs.idlesObj };
                    triggerEvent.Events.Add(vrcEvent);
                    trigger.Triggers.Add(triggerEvent);

                    triggerEvent = new VRC_Trigger.TriggerEvent();
                    triggerEvent.TriggerType = VRC_Trigger.TriggerType.OnPlayerLeft;
                    triggerEvent.BroadcastType = VRC_EventHandler.VrcBroadcastType.MasterUnbuffered;
                    vrcEvent = new VRC_EventHandler.VrcEvent();
                    vrcEvent.EventType = VRC_EventHandler.VrcEventType.SendRPC;
                    vrcEvent.ParameterString = "Play";
                    vrcEvent.ParameterInt = 6;
                    vrcEvent.ParameterBytes = VRC_Serialization.ParameterEncoder(new object[] { 0 });
                    vrcEvent.ParameterObjects = new GameObject[] { config.configInfo.eventObjs.idlesObj };
                    triggerEvent.Events.Add(vrcEvent);
                    trigger.Triggers.Add(triggerEvent);

                    audioBank.Clips = new AudioClip[] { AssetDatabase.LoadAssetAtPath<AudioClip>(FigureAvatarUtil.soundlessAudioClipPath) };
                    AudioSource audioBankaudioSource = config.configInfo.eventObjs.idlesObj.GetOrAddComponent<AudioSource>();
                    audioBank.Source = audioBankaudioSource;
                    VRC_Trigger.CustomTriggerTarget triggerTarget = new VRC_Trigger.CustomTriggerTarget();
                    triggerTarget.TriggerObject = config.configInfo.eventObjs.idlesObj;
                    triggerTarget.CustomName = "MasterLocal";
                    audioBank.OnPlay = triggerTarget;
                }
                catch (System.NullReferenceException)
                {
                    FigureAvatarUtil.ErrorMessage("プレハブの編集に失敗しました。");
                    Destructor();
                    return;
                }
            }

            AssetDatabase.SaveAssets();
            EditorUtility.SetDirty(config);
            EditorUtility.SetDirty(animatorController);
            animator.runtimeAnimatorController = animatorController;

            /*avatarInfo.boneInfoList = config.boneInfoList;
            avatarInfo.idleInfo = config.idleInfo;
            avatarInfo.approachInfo = config.approachInfo;
            avatarInfo.eventObjs = config.configInfo.eventObjs;*/

            config.configInfo.prefabName = System.IO.Path.GetFileName(outPath);
            System.Type prefabUtilityType = typeof(PrefabUtility);
            try
            {
                System.Type unityEditorType = System.Reflection.Assembly.GetExecutingAssembly().GetType();
                System.Reflection.FieldInfo interactionModeField = unityEditorType.GetField("InteractionMode");
                System.Reflection.MethodInfo saveAsPrefabAssetAndConnectMethodInfo = prefabUtilityType.GetMethod("SaveAsPrefabAssetAndConnect", System.Reflection.BindingFlags.Static);
                saveAsPrefabAssetAndConnectMethodInfo.Invoke(null, new object[] { config.configInfo.eventObjs.prefabObj, outPath, System.Enum.GetValues(interactionModeField.GetType()).GetValue(1) });
            }
            catch
            {
                System.Reflection.MethodInfo createPrefabMethodInfo = prefabUtilityType.GetMethod("CreatePrefab", new System.Type[] { typeof(string), typeof(GameObject) });
                object prefab = createPrefabMethodInfo.Invoke(null, new object[] { outPath, config.configInfo.eventObjs.prefabObj });
                System.Reflection.MethodInfo connectGameObjectToPrefabMethodInfo = prefabUtilityType.GetMethod("ConnectGameObjectToPrefab", new System.Type[] { typeof(GameObject), typeof(GameObject) });
                connectGameObjectToPrefabMethodInfo.Invoke(null, new object[] { config.configInfo.eventObjs.prefabObj, prefab });
            }
            AssetDatabase.Refresh();
        }

        private void SetClip(Animator animator, UnityEditor.Animations.AnimatorController animatorController, VRC_Trigger.TriggerEvent triggerEvent, EventObjects objs, AudioSource audioSource, ClipInfo clipInfo, string animTriggerName, bool idleFlag = false, UnityEditor.Animations.AnimatorState idleState = null, UnityEditor.Animations.AnimatorState idleState2 = null)
        {
            UnityEditor.Animations.AnimatorStateMachine stateMachine = animatorController.layers[0].stateMachine;

            if (clipInfo.animClip != null || clipInfo.audioClip != null)
            {
                GameObject buttonObj = new GameObject("Button");
                buttonObj.transform.SetParent(config.configInfo.eventObjs.buttonsObj.transform);
                TransformReset(buttonObj.transform);
                AddPressAnimator(buttonObj, FigureAvatarUtil.pressManualAnimatorPath);
                Button button = buttonObj.AddComponent<Button>();
                button.transition = Selectable.Transition.None;
                button.navigation = nav;
                UnityEventUtil.UnityEventCalls calls = UnityEventUtil.GetButtonEventFields(button);
                List<UnityEventUtil.UnityEventCalls.Fields> fieldList = new List<UnityEventUtil.UnityEventCalls.Fields>();
                if (clipInfo.animClip != null)
                {
                    if (animTriggerName != "")
                    {
                        animatorController.AddParameter(animTriggerName, UnityEngine.AnimatorControllerParameterType.Trigger);
                        UnityEditor.Animations.AnimatorState state = stateMachine.AddState(clipInfo.animClip.name, idleFlag ? stateMachine.states.Length > 0 ? stateMachine.states[stateMachine.states.Length - 1].position + new Vector3(0, 100, 0) : new Vector3(0, 200, 0) : stateMachine.states.Length > 0 ? stateMachine.states[stateMachine.states.Length - 1].position + new Vector3(50, 50, 0) : new Vector3(240, 120, 0));
                        state.motion = clipInfo.animClip;
                        UnityEditor.Animations.AnimatorStateTransition exitTransition = state.AddExitTransition();
                        exitTransition.hasExitTime = true;
                        exitTransition.exitTime = 1;
                        exitTransition.duration = 0.2f;
                        if (idleFlag && idleState != null && idleState2 != null)
                        {
                            UnityEditor.Animations.AnimatorStateTransition transition = idleState.AddTransition(state);
                            transition.hasExitTime = false;
                            transition.duration = 0.2f;
                            transition.canTransitionToSelf = false;

                            UnityEditor.Animations.AnimatorStateTransition transition2 = idleState2.AddTransition(state);
                            transition2.hasExitTime = false;
                            transition2.duration = 0.2f;
                            transition2.canTransitionToSelf = false;

                            UnityEditor.Animations.AnimatorCondition condition = new UnityEditor.Animations.AnimatorCondition();
                            condition.mode = UnityEditor.Animations.AnimatorConditionMode.If;
                            condition.parameter = animTriggerName;
                            condition.threshold = 0;

                            transition.conditions = new UnityEditor.Animations.AnimatorCondition[] { condition };
                            transition2.conditions = new UnityEditor.Animations.AnimatorCondition[] { condition };
                        }
                        else if (!idleFlag)
                        {
                            UnityEditor.Animations.AnimatorStateTransition anyTransition = stateMachine.AddAnyStateTransition(state);
                            anyTransition.hasExitTime = false;
                            anyTransition.duration = 0.2f;
                            anyTransition.canTransitionToSelf = true;
                            UnityEditor.Animations.AnimatorCondition condition = new UnityEditor.Animations.AnimatorCondition();
                            condition.mode = UnityEditor.Animations.AnimatorConditionMode.If;
                            condition.parameter = animTriggerName;
                            condition.threshold = 0;
                            anyTransition.conditions = new UnityEditor.Animations.AnimatorCondition[] { condition };
                        }
                    }

                    GameObject clipObj = new GameObject("Clip");
                    clipObj.transform.SetParent(config.configInfo.eventObjs.animClipsObj.transform);
                    TransformReset(clipObj.transform);
                    AddPressButton(clipObj, new UnityEventUtil.UnityEventCalls.Fields[] { new UnityEventUtil.UnityEventCalls.Fields(UnityEngine.Events.PersistentListenerMode.String, "SetTrigger", config.configInfo.eventObjs.prefabObj.GetComponent<Animator>(), UnityEngine.Events.UnityEventCallState.RuntimeOnly, new UnityEventUtil.UnityEventCalls.Arguments(null, "", 0, 0, animTriggerName)) }, FigureAvatarUtil.pressManualAnimatorPath);
                    fieldList.Add(new UnityEventUtil.UnityEventCalls.Fields(UnityEngine.Events.PersistentListenerMode.String, "SetTrigger", clipObj.GetComponent<Animator>(), UnityEngine.Events.UnityEventCallState.RuntimeOnly, new UnityEventUtil.UnityEventCalls.Arguments(null, "", 0, 0, "Trigger")));
                }
                if (clipInfo.audioClip != null)
                {
                    GameObject clipObj = new GameObject("Clip");
                    clipObj.transform.SetParent(config.configInfo.eventObjs.audioClipsObj.transform);
                    TransformReset(clipObj.transform);
                    AddPressButton(clipObj, new UnityEventUtil.UnityEventCalls.Fields[]
                    {
                        new UnityEventUtil.UnityEventCalls.Fields(UnityEngine.Events.PersistentListenerMode.Void, "Stop", audioSource, UnityEngine.Events.UnityEventCallState.RuntimeOnly, new UnityEventUtil.UnityEventCalls.Arguments(null, "", 0, 0, "")),
                        new UnityEventUtil.UnityEventCalls.Fields(UnityEngine.Events.PersistentListenerMode.Object, "PlayOneShot", audioSource, UnityEngine.Events.UnityEventCallState.RuntimeOnly, new UnityEventUtil.UnityEventCalls.Arguments(clipInfo.audioClip, "UnityEngine.AudioClip, UnityEngine", 0, 0, ""))
                    }, FigureAvatarUtil.pressManualAnimatorPath);
                    fieldList.Add(new UnityEventUtil.UnityEventCalls.Fields(UnityEngine.Events.PersistentListenerMode.String, "SetTrigger", clipObj.GetComponent<Animator>(), UnityEngine.Events.UnityEventCallState.RuntimeOnly, new UnityEventUtil.UnityEventCalls.Arguments(null, "", 0, 0, "Trigger")));
                }

                if (config.typesFlag)
                {
                    if (!clipInfo.lookFlag)
                    {
                        Transform headTransform = animator.GetBoneTransform(HumanBodyBones.Head);
                        Transform neckTransform = animator.GetBoneTransform(HumanBodyBones.Neck);
                        Animator lookatManagerAnimator = headTransform.gameObject.GetComponent<Animator>();
                        Animator neckLookatManagerAnimator = neckTransform.gameObject.GetComponent<Animator>();

                        GameObject RetentionObj = new GameObject("RetentionLookat");
                        RetentionObj.transform.SetParent(buttonObj.transform);
                        TransformReset(RetentionObj.transform);
                        RetentionObj.SetActive(false);
                        GameObject trueObj = new GameObject("True");
                        trueObj.transform.SetParent(RetentionObj.transform);
                        TransformReset(trueObj.transform);
                        trueObj.SetActive(false);
                        AddPressButton(trueObj, new UnityEventUtil.UnityEventCalls.Fields[]
                        {
                            new UnityEventUtil.UnityEventCalls.Fields(UnityEngine.Events.PersistentListenerMode.String, "SetTrigger", lookatManagerAnimator, UnityEngine.Events.UnityEventCallState.RuntimeOnly, new UnityEventUtil.UnityEventCalls.Arguments(null, "", 0, 0, "Lookat")),
                            new UnityEventUtil.UnityEventCalls.Fields(UnityEngine.Events.PersistentListenerMode.String, "SetTrigger", neckLookatManagerAnimator, UnityEngine.Events.UnityEventCallState.RuntimeOnly, new UnityEventUtil.UnityEventCalls.Arguments(null, "", 0, 0, "Lookat")),
                            new UnityEventUtil.UnityEventCalls.Fields(UnityEngine.Events.PersistentListenerMode.Bool, "SetActive", RetentionObj, UnityEngine.Events.UnityEventCallState.RuntimeOnly, new UnityEventUtil.UnityEventCalls.Arguments(null, "", 0, 0, "", false))
                        }, FigureAvatarUtil.pressAnimatorPath);
                        GameObject falseObj = new GameObject("False");
                        falseObj.transform.SetParent(RetentionObj.transform);
                        TransformReset(falseObj.transform);
                        falseObj.SetActive(false);
                        AddPressButton(falseObj, new UnityEventUtil.UnityEventCalls.Fields[]
                        {
                            new UnityEventUtil.UnityEventCalls.Fields(UnityEngine.Events.PersistentListenerMode.String, "ResetTrigger", lookatManagerAnimator, UnityEngine.Events.UnityEventCallState.RuntimeOnly, new UnityEventUtil.UnityEventCalls.Arguments(null, "", 0, 0, "Lookat")),
                            new UnityEventUtil.UnityEventCalls.Fields(UnityEngine.Events.PersistentListenerMode.String, "ResetTrigger", neckLookatManagerAnimator, UnityEngine.Events.UnityEventCallState.RuntimeOnly, new UnityEventUtil.UnityEventCalls.Arguments(null, "", 0, 0, "Lookat")),
                            new UnityEventUtil.UnityEventCalls.Fields(UnityEngine.Events.PersistentListenerMode.Bool, "SetActive", RetentionObj, UnityEngine.Events.UnityEventCallState.RuntimeOnly, new UnityEventUtil.UnityEventCalls.Arguments(null, "", 0, 0, "", false))
                        }, FigureAvatarUtil.pressAnimatorPath);
                        GameObject timerObj = new GameObject("Timer");
                        timerObj.transform.SetParent(buttonObj.transform);
                        TransformReset(timerObj.transform);
                        timerObj.SetActive(false);
                        Component timedObjectActivator = timerObj.AddComponent(types.timedObjectActivatorType);
                        System.Reflection.FieldInfo fieldInfo = types.timedObjectActivatorType.GetField("entries");
                        object entries = fieldInfo.GetValue(timedObjectActivator);
                        System.Reflection.FieldInfo fieldInfo2 = entries.GetType().GetField("entries");
                        System.Array entries2 = (System.Array) System.Activator.CreateInstance(fieldInfo2.FieldType, 2);
                        object entry = System.Activator.CreateInstance(entries2.GetType().GetElementType());
                        object entry2 = System.Activator.CreateInstance(entries2.GetType().GetElementType());
                        System.Reflection.FieldInfo targetFieldInfo = entry.GetType().GetField("target");
                        System.Reflection.FieldInfo actionFieldInfo = entry.GetType().GetField("action");
                        System.Reflection.FieldInfo delayFieldInfo = entry.GetType().GetField("delay");
                        targetFieldInfo.SetValue(entry, RetentionObj);
                        actionFieldInfo.SetValue(entry, 0);
                        delayFieldInfo.SetValue(entry, clipInfo.animClip.length);
                        targetFieldInfo.SetValue(entry2, timerObj);
                        actionFieldInfo.SetValue(entry2, 1);
                        delayFieldInfo.SetValue(entry2, clipInfo.animClip.length);
                        entries2.SetValue(entry, 0);
                        entries2.SetValue(entry2, 1);
                        fieldInfo2.SetValue(entries, entries2);
                        AddPressButton(timerObj, new UnityEventUtil.UnityEventCalls.Fields[] { new UnityEventUtil.UnityEventCalls.Fields(UnityEngine.Events.PersistentListenerMode.Void, "Awake", timedObjectActivator, UnityEngine.Events.UnityEventCallState.RuntimeOnly, new UnityEventUtil.UnityEventCalls.Arguments(null, "", 0, 0, "")) }, FigureAvatarUtil.pressAnimatorPath);
                        GameObject switchObj = new GameObject("Switch");
                        switchObj.transform.SetParent(buttonObj.transform);
                        TransformReset(switchObj.transform);
                        InputField inputField = switchObj.AddComponent<InputField>();
                        inputField.transition = Selectable.Transition.None;
                        inputField.navigation = nav;
                        inputField.textComponent = config.configInfo.eventObjs.textObj.GetComponent<Text>();
                        UnityEventUtil.UnityEventCalls inputFieldOnValueChangedCalls = UnityEventUtil.GetInputFieldOnValueChangedEventFields(inputField);
                        UnityEventUtil.UnityEventCalls inputFieldOnEndEditCalls = UnityEventUtil.GetInputFieldOnEndEditEventFields(inputField);
                        inputFieldOnValueChangedCalls.fields = new UnityEventUtil.UnityEventCalls.Fields[]
                        {
                            new UnityEventUtil.UnityEventCalls.Fields(UnityEngine.Events.PersistentListenerMode.Bool, "SetActive", trueObj, UnityEngine.Events.UnityEventCallState.RuntimeOnly, new UnityEventUtil.UnityEventCalls.Arguments(null, "", 0, 0, "", true)),
                            new UnityEventUtil.UnityEventCalls.Fields(UnityEngine.Events.PersistentListenerMode.Bool, "SetActive", falseObj, UnityEngine.Events.UnityEventCallState.RuntimeOnly, new UnityEventUtil.UnityEventCalls.Arguments(null, "", 0, 0, "", false))
                        };
                        inputFieldOnEndEditCalls.fields = new UnityEventUtil.UnityEventCalls.Fields[]
                        {
                            new UnityEventUtil.UnityEventCalls.Fields(UnityEngine.Events.PersistentListenerMode.Bool, "SetActive", trueObj, UnityEngine.Events.UnityEventCallState.RuntimeOnly, new UnityEventUtil.UnityEventCalls.Arguments(null, "", 0, 0, "", false)),
                            new UnityEventUtil.UnityEventCalls.Fields(UnityEngine.Events.PersistentListenerMode.Bool, "SetActive", falseObj, UnityEngine.Events.UnityEventCallState.RuntimeOnly, new UnityEventUtil.UnityEventCalls.Arguments(null, "", 0, 0, "", true))
                        };
                        UnityEventUtil.SetInputFieldOnValueChangedEventFields(inputField, inputFieldOnValueChangedCalls);
                        UnityEventUtil.SetInputFieldOnEndEditEventFields(inputField, inputFieldOnEndEditCalls);

                        fieldList.Add(new UnityEventUtil.UnityEventCalls.Fields(UnityEngine.Events.PersistentListenerMode.String, "ResetTrigger", lookatManagerAnimator, UnityEngine.Events.UnityEventCallState.RuntimeOnly, new UnityEventUtil.UnityEventCalls.Arguments(null, "", 0, 0, "Lookat")));
                        fieldList.Add(new UnityEventUtil.UnityEventCalls.Fields(UnityEngine.Events.PersistentListenerMode.String, "ResetTrigger", neckLookatManagerAnimator, UnityEngine.Events.UnityEventCallState.RuntimeOnly, new UnityEventUtil.UnityEventCalls.Arguments(null, "", 0, 0, "Lookat")));
                        fieldList.Add(new UnityEventUtil.UnityEventCalls.Fields(UnityEngine.Events.PersistentListenerMode.Bool, "SetActive", timerObj, UnityEngine.Events.UnityEventCallState.RuntimeOnly, new UnityEventUtil.UnityEventCalls.Arguments(null, "", 0, 0, "", true)));
                    }
                }

                calls.fields = fieldList.ToArray();
                UnityEventUtil.SetButtonEventFields(button, calls);

                VRC_EventHandler.VrcEvent vrcEvent = new VRC_EventHandler.VrcEvent();
                vrcEvent.EventType = VRC_EventHandler.VrcEventType.AnimationTrigger;
                vrcEvent.ParameterString = "Trigger";
                vrcEvent.ParameterObjects = new GameObject[] { buttonObj };
                triggerEvent.Events.Add(vrcEvent);
            }
        }

        private void AddPressButton(GameObject obj, UnityEventUtil.UnityEventCalls.Fields[] fields, string controllerPath)
        {
            AddPressAnimator(obj, controllerPath);
            Button button = obj.GetOrAddComponent<Button>();
            SetButtonEvents(button, fields);
        }

        private void SetButtonEvents(Button button, UnityEventUtil.UnityEventCalls.Fields[] fields)
        {
            button.transition = Selectable.Transition.None;
            button.navigation = nav;
            UnityEventUtil.UnityEventCalls calls = UnityEventUtil.GetButtonEventFields(button);
            calls.fields = fields;
            UnityEventUtil.SetButtonEventFields(button, calls);
        }

        private Animator AddPressAnimator(GameObject obj, string controllerPath)
        {
            Animator animator = obj.GetOrAddComponent<Animator>();
            animator.runtimeAnimatorController = AssetDatabase.LoadAssetAtPath<RuntimeAnimatorController>(controllerPath);
            animator.cullingMode = AnimatorCullingMode.AlwaysAnimate;
            return animator;
        }

        private void TransformReset(Transform t)
        {
            t.localPosition = Vector3.zero;
            t.localRotation = Quaternion.Euler(0, 0, 0);
            t.localScale = Vector3.one;
        }

        private VRC_Trigger.TriggerEvent SetProbabilitie(VRC_Trigger.TriggerEvent triggerEvent, float[] probabilities, bool Lock)
        {
            triggerEvent.Probabilities = probabilities;
            triggerEvent.ProbabilityLock = (new bool[probabilities.Length]).Select(b => Lock).ToArray();
            return triggerEvent;
        }

        private void Destructor()
        {
            
        }

        private void StandardAssetLoad()
        {
            config.typesFlag = false;

            foreach (System.Reflection.Assembly assembly in System.AppDomain.CurrentDomain.GetAssemblies())
            {
                foreach (System.Type type in assembly.GetTypes())
                {
                    if (type.Name == "LookatTarget")
                    {
                        types.lookatTargetType = type;
                    }
                    else if (type.Name == "AutoCam")
                    {
                        types.autoCamType = type;
                    }
                    else if (type.Name == "TimedObjectActivator")
                    {
                        types.timedObjectActivatorType = type;
                    }
                }

                if (types.lookatTargetType != null && types.autoCamType != null && types.timedObjectActivatorType != null)
                {
                    config.typesFlag = true;
                    break;
                }
            }
        }
    }

    internal class NewPresetWindow : EditorWindow
    {
        internal static readonly Vector2 minWindowSize = new Vector2(300, 120);
        internal static readonly Vector2 maxWindowSize = new Vector2(300.1f, 120.1f);

        private static FigureAvatarSetupWindow parent;
        private string presetName = "";

        public static void ShowWindow(FigureAvatarSetupWindow _parent)
        {
            NewPresetWindow window = GetWindow<NewPresetWindow>(true);
            window.titleContent.text = "Set Preset";
            window.maxSize = maxWindowSize;
            window.minSize = minWindowSize;
            parent = _parent;
        }

        private void OnGUI()
        {
            string tmp_presetName = "";

            Focus();

            EditorGUILayout.Space();
            EditorGUILayout.BeginVertical();
            {
                GUILayout.Space(maxWindowSize.y / 2 - 30);

                EditorGUILayout.BeginHorizontal();
                {
                    EditorGUI.BeginChangeCheck();
                    {
                        EditorGUILayout.LabelField("Preset Name:", GUILayout.Width(80));
                        tmp_presetName = EditorGUILayout.TextField(presetName);
                    }
                    if (EditorGUI.EndChangeCheck())
                    {
                        presetName = tmp_presetName;
                    }
                }
                EditorGUILayout.EndHorizontal();

                GUILayout.FlexibleSpace();

                EditorGUILayout.BeginHorizontal();
                {
                    EditorGUI.BeginDisabledGroup(presetName == "" || presetName == null);
                    {
                        if (GUILayout.Button("OK", FigureAvatarUtil.Styles.ButtonBig))
                        {
                            if (presetName != null && parent != null && parent.config != null && parent.preset != null && parent.preset.presetList != null)
                            {
                                PresetItem item = new PresetItem();
                                item.presetName = presetName;
                                item.config = parent.config.configInfo.Clone();
                                parent.preset.presetList.Add(item);
                                AssetDatabase.SaveAssets();
                                EditorUtility.SetDirty(parent.preset);
                                Close();
                            }
                        }
                    }
                    EditorGUI.EndDisabledGroup();

                    if (GUILayout.Button("キャンセル", FigureAvatarUtil.Styles.ButtonBig))
                    {
                        Close();
                    }
                }
                EditorGUILayout.EndHorizontal();
            }
            EditorGUILayout.EndVertical();

            EditorGUILayout.Space();
        }
    }
}
