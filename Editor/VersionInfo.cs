﻿using UnityEngine;

namespace FigureAvatar
{
    public class VersionInfo : ScriptableObject
    {
        public string version;
    }
}
