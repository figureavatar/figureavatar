﻿using System.Collections.Generic;
using UnityEngine;

namespace FigureAvatar
{
    public class PresetInfo : ScriptableObject
    {
        public int selectPresetIndex = 0;
        public List<PresetItem> presetList = new List<PresetItem>();
    }

    [System.Serializable]
    public class PresetItem
    {
        public string presetName;
        public ConfigInfo config = new ConfigInfo();
    }
}