﻿using System.Collections;
using System.IO;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;
using UnityEngine.Networking;

namespace FigureAvatar
{
    public class FigureAvatarVersion : EditorWindow
    {
        internal static readonly Vector2 minWindowSize = new Vector2(368, 220);
        internal static readonly Vector2 maxWindowSize = new Vector2(368.1f, 220.1f);

        private const string packagesFolderPath = "FigureAvatarPackages";

        private string newVersion = "";

        [MenuItem("FigureAvatar/Version")]
        internal static void ShowSetupWindow()
        {
            FigureAvatarVersion window = GetWindow<FigureAvatarVersion>();
            window.minSize = minWindowSize;
            window.maxSize = maxWindowSize;
        }

        private void OnGUI()
        {
            VersionInfo versionInfo = FigureAvatarUtil.VersionLoad();
            if (versionInfo == null)
            {
                FigureAvatarUtil.ErrorMessage("バージョンファイルが見つかりませんでした。再インポートしてください。");
                return;
            }

            Material mat = AssetDatabase.LoadAssetAtPath<Material>(FigureAvatarUtil.GetMainFolderPath() + "/Materials/Texture.mat");
            Texture2D logo = AssetDatabase.LoadAssetAtPath<Texture2D>(FigureAvatarUtil.GetMainFolderPath() + "/Textures/logo.png");
            Texture2D cc0 = AssetDatabase.LoadAssetAtPath<Texture2D>(FigureAvatarUtil.GetMainFolderPath() + "/Textures/cc0.png");
            EditorGUI.DrawPreviewTexture(new Rect(0, 0, 184, 184), logo, mat);
            EditorGUI.DrawPreviewTexture(new Rect(184, 100, 88, 31), cc0, mat);
            EditorGUI.LabelField(new Rect(184, 30, 184, 15), "Version:" + versionInfo.version);
            EditorGUI.LabelField(new Rect(184, 80, 184, 15), "LICENSE: CC0");
            GUILayout.FlexibleSpace();
            if (GUILayout.Button("Update Check", FigureAvatarUtil.Styles.ButtonBig))
            {
                UpdateCheck();
            }
            EditorGUILayout.Space();
        }

        private void OnFocus()
        {
            CompatibleConvert();
        }

        internal static void CompatibleConvert()
        {
            string beta131ControllersFolderPath = FigureAvatarUtil.GetMainFolderPath() + "/Controllers";
            string beta140ControllersFolderPath = FigureAvatarUtil.controllersFolderPath;
            if (Directory.Exists(beta131ControllersFolderPath))
            {
                AssetDatabase.DeleteAsset(beta131ControllersFolderPath + "/Dummy.controller");
                Debug.Log(beta131ControllersFolderPath + ":" + beta140ControllersFolderPath);
                Debug.Log(AssetDatabase.CopyAsset(beta131ControllersFolderPath, beta140ControllersFolderPath));
                if (AssetDatabase.CopyAsset(beta131ControllersFolderPath, beta140ControllersFolderPath))
                {
                    AssetDatabase.DeleteAsset(beta131ControllersFolderPath);
                }
            }
        }

        private void UpdateCheck()
        {
            VersionInfo versionInfo = FigureAvatarUtil.VersionLoad();
            IEnumerator releases = GetReleases();
            while (releases.MoveNext()) { }
            if (versionInfo == null)
            {
                FigureAvatarUtil.ErrorMessage("バージョンファイルが見つかりませんでした。再インポートしてください。");
                return;
            }
            if (releases.Current == null)
            {
                FigureAvatarUtil.ErrorMessage("現在の最新バージョンの取得に失敗しました。");
                return;
            }
            ReleaseData data = JsonUtility.FromJson<ReleaseData>("{\"data\": " + releases.Current.ToString() + "}");
            if (data == null || data.data == null || data.data.Length == 0)
            {
                FigureAvatarUtil.ErrorMessage("アップデートバージョンの取得に失敗しました。");
                return;
            }
            if (data.data[0].tag_name == versionInfo.version)
            {
                FigureAvatarUtil.MessageBox("No Update", "更新の必要はありません。", FigureAvatarUtil.MessageType.OK);
                return;
            }
            newVersion = data.data[0].tag_name;
            Regex regex = new Regex(@"\(\/uploads\/(?<value>.*)\)");
            Match match = regex.Match(data.data[0].description);
            string url = "https://gitlab.com/figureavatar/figureavatar/uploads/" + match.Groups["value"].Value;
            string packagePath = packagesFolderPath + "/" + Path.GetFileName(url);

            IEnumerator download = DownloadPackage(url, packagePath);
            try
            {
                if (FigureAvatarUtil.MessageBox("Update", "最新バージョン「" + data.data[0].tag_name + "」がリリースされています。更新しますか？", FigureAvatarUtil.MessageType.YesNo))
                {
                    while (download.MoveNext()) { }

                    AssetDatabase.importPackageCompleted += ImportCompleted;
                    AssetDatabase.ImportPackage(packagePath, true);
                }
            }
            catch
            {
                FigureAvatarUtil.ErrorMessage("UnityPackageを開けませんでした。");
                return;
            }
        }

        private void ImportCompleted(string packageName)
        {
            AssetDatabase.importPackageCompleted -= ImportCompleted;
            FigureAvatarUtil.MessageBox("Complete!", "バージョン「" + newVersion + "」へのアップデートが完了しました！", FigureAvatarUtil.MessageType.OK);
        }

        private static IEnumerator GetReleases()
        {
            using (UnityWebRequest request = UnityWebRequest.Get("https://gitlab.com/api/v4/projects/15021672/releases"))
            {
                request.SendWebRequest();
                while (!request.isDone) yield return null;
                if (request.isNetworkError || request.isHttpError) yield break;
                yield return request.downloadHandler.text;
            }
        }

        private static IEnumerator DownloadPackage(string url, string packagePath)
        {
            using (UnityWebRequest request = UnityWebRequest.Get(url))
            {
                request.SendWebRequest();
                while (!request.isDone)
                {
                    float progress = request.downloadProgress;
                    EditorUtility.DisplayProgressBar("Downloading...", (progress * 100).ToString("F2") + "%", progress);
                }
                if (request.isNetworkError || request.isHttpError) yield break;
                File.WriteAllBytes(packagePath, request.downloadHandler.data);
            }
        }

        [System.Serializable]
        private class ReleaseData
        {
            public Data[] data;
            [System.Serializable]
            public class Data
            {
                public string tag_name;
                public string description;
            }
        }
    }
}