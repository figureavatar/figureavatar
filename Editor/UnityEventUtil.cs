﻿using UnityEditor;
using UnityEngine.Events;
using UnityEngine.UI;

namespace FigureAvatar
{
    public static class UnityEventUtil
    {
        public static UnityEventCalls GetInputFieldOnValueChangedEventFields(InputField inputField)
        {
            SerializedObject serializedObject = new SerializedObject(inputField);
            SerializedProperty prop = serializedObject.FindProperty("m_OnValueChanged");
            return GetUnityEventFields(serializedObject, prop);
        }

        public static void SetInputFieldOnValueChangedEventFields(InputField inputField, UnityEventCalls calls)
        {
            SerializedObject serializedObject = new SerializedObject(inputField);
            SerializedProperty prop = serializedObject.FindProperty("m_OnValueChanged");
            SetUnityEventFields(prop, calls);
        }

        public static UnityEventCalls GetInputFieldOnEndEditEventFields(InputField inputField)
        {
            SerializedObject serializedObject = new SerializedObject(inputField);
            SerializedProperty prop = serializedObject.FindProperty("m_OnEndEdit");
            return GetUnityEventFields(serializedObject, prop);
        }

        public static void SetInputFieldOnEndEditEventFields(InputField inputField, UnityEventCalls calls)
        {
            SerializedObject serializedObject = new SerializedObject(inputField);
            SerializedProperty prop = serializedObject.FindProperty("m_OnEndEdit");
            SetUnityEventFields(prop, calls);
        }

        public static UnityEventCalls GetButtonEventFields(Button button)
        {
            SerializedObject serializedObject = new SerializedObject(button);
            SerializedProperty prop = serializedObject.FindProperty("m_OnClick");
            return GetUnityEventFields(serializedObject, prop);
        }

        public static void SetButtonEventFields(Button button, UnityEventCalls calls)
        {
            SerializedObject serializedObject = new SerializedObject(button);
            SerializedProperty prop = serializedObject.FindProperty("m_OnClick");
            SetUnityEventFields(prop, calls);
        }

        public static UnityEventCalls GetUnityEventFields(SerializedObject serializedObject, SerializedProperty prop)
        {
            SerializedProperty persistentProp = prop.FindPropertyRelative("m_PersistentCalls");
            SerializedProperty callsProp = persistentProp.FindPropertyRelative("m_Calls");

            UnityEventCalls result = new UnityEventCalls(serializedObject, new UnityEventCalls.Fields[callsProp.arraySize]);
            for (int i = 0; i < callsProp.arraySize; i++)
            {
                SerializedProperty elementProp = callsProp.GetArrayElementAtIndex(i);
                SerializedProperty targetProp = elementProp.FindPropertyRelative("m_Target");
                SerializedProperty modeProp = elementProp.FindPropertyRelative("m_Mode");
                SerializedProperty methodProp = elementProp.FindPropertyRelative("m_MethodName");
                SerializedProperty argumentsProp = elementProp.FindPropertyRelative("m_Arguments");
                SerializedProperty callStateProp = elementProp.FindPropertyRelative("m_CallState");
                SerializedProperty objectProp = argumentsProp.FindPropertyRelative("m_ObjectArgument");
                SerializedProperty objectTypeNameProp = argumentsProp.FindPropertyRelative("m_ObjectArgumentAssemblyTypeName");
                SerializedProperty intProp = argumentsProp.FindPropertyRelative("m_IntArgument");
                SerializedProperty floatProp = argumentsProp.FindPropertyRelative("m_FloatArgument");
                SerializedProperty stringProp = argumentsProp.FindPropertyRelative("m_StringArgument");
                SerializedProperty boolProp = argumentsProp.FindPropertyRelative("m_BoolArgument");
                result.fields[i] = new UnityEventCalls.Fields((PersistentListenerMode)modeProp.intValue, methodProp.stringValue, targetProp.objectReferenceValue, (UnityEventCallState)callStateProp.intValue, new UnityEventCalls.Arguments(objectProp.objectReferenceValue, objectTypeNameProp.stringValue, intProp.intValue, floatProp.floatValue, stringProp.stringValue, boolProp.boolValue));
            }

            return result;
        }

        public static void SetUnityEventFields(SerializedProperty prop, UnityEventCalls calls)
        {
            SerializedProperty persistentProp = prop.FindPropertyRelative("m_PersistentCalls");
            SerializedProperty callsProp = persistentProp.FindPropertyRelative("m_Calls");
            callsProp.arraySize = calls.fields.Length;

            for (int i = 0; i < callsProp.arraySize; i++)
            {
                SerializedProperty elementProp = callsProp.GetArrayElementAtIndex(i);
                SerializedProperty targetProp = elementProp.FindPropertyRelative("m_Target");
                SerializedProperty modeProp = elementProp.FindPropertyRelative("m_Mode");
                SerializedProperty methodProp = elementProp.FindPropertyRelative("m_MethodName");
                SerializedProperty argumentsProp = elementProp.FindPropertyRelative("m_Arguments");
                SerializedProperty callStateProp = elementProp.FindPropertyRelative("m_CallState");
                SerializedProperty objectProp = argumentsProp.FindPropertyRelative("m_ObjectArgument");
                SerializedProperty objectTypeNameProp = argumentsProp.FindPropertyRelative("m_ObjectArgumentAssemblyTypeName");
                SerializedProperty intProp = argumentsProp.FindPropertyRelative("m_IntArgument");
                SerializedProperty floatProp = argumentsProp.FindPropertyRelative("m_FloatArgument");
                SerializedProperty stringProp = argumentsProp.FindPropertyRelative("m_StringArgument");
                SerializedProperty boolProp = argumentsProp.FindPropertyRelative("m_BoolArgument");
                targetProp.objectReferenceValue = calls.fields[i].target;
                modeProp.intValue = (int)calls.fields[i].mode;
                methodProp.stringValue = calls.fields[i].methodName;
                callStateProp.intValue = (int)calls.fields[i].callState;
                objectProp.objectReferenceValue = calls.fields[i].arguments.objectArgument;
                objectTypeNameProp.stringValue = calls.fields[i].arguments.objectArgumentAssemblyTypeName;
                intProp.intValue = calls.fields[i].arguments.intArgument;
                floatProp.floatValue = calls.fields[i].arguments.floatArgument;
                stringProp.stringValue = calls.fields[i].arguments.stringArgument;
                boolProp.boolValue = calls.fields[i].arguments.boolArgument;
                prop.serializedObject.ApplyModifiedProperties();
            }
        }

        public class UnityEventCalls
        {
            public SerializedObject serializedObject;
            public Fields[] fields;

            public UnityEventCalls(SerializedObject _serializedObject, Fields[] _fields)
            {
                this.serializedObject = _serializedObject;
                this.fields = _fields;
            }

            public class Fields
            {
                public PersistentListenerMode mode;
                public string methodName;
                public UnityEngine.Object target;
                public UnityEventCallState callState;
                public Arguments arguments;

                public Fields(PersistentListenerMode _mode, string _methodName, UnityEngine.Object _target, UnityEventCallState _callState, Arguments _arugments)
                {
                    this.mode = _mode;
                    this.methodName = _methodName;
                    this.target = _target;
                    this.callState = _callState;
                    this.arguments = _arugments;
                }
            }

            public class Arguments
            {
                public UnityEngine.Object objectArgument;
                public string objectArgumentAssemblyTypeName;
                public int intArgument;
                public float floatArgument;
                public string stringArgument;
                public bool boolArgument;

                public Arguments(UnityEngine.Object _objectArgument = null, string _objectArgumentAssemblyTypeName = "", int _intArgument = 0, float _floatArgument = 0, string _stringArgument = "", bool _boolArgument = false)
                {
                    this.objectArgument = _objectArgument;
                    this.objectArgumentAssemblyTypeName = _objectArgumentAssemblyTypeName;
                    this.intArgument = _intArgument;
                    this.floatArgument = _floatArgument;
                    this.stringArgument = _stringArgument;
                    this.boolArgument = _boolArgument;
                }
            }
        }
    }
}