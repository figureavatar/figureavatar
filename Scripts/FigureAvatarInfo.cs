﻿#if UNITY_EDITOR
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace FigureAvatar
{
    public class FigureAvatarInfo : MonoBehaviour
    {
        public ConfigInfo config = new ConfigInfo();
    }

    [System.Serializable]
    public class BoneInfo : InfoBase
    {
        public HumanBodyBones bone = HumanBodyBones.Hips;
        public float colSize = 0.1f;
        public Vector3 offset = Vector3.zero;

        public BoneInfo() { }

        public BoneInfo(HumanBodyBones _bone)
        {
            bone = _bone;
        }

        public BoneInfo Clone()
        {
            BoneInfo result = (BoneInfo) MemberwiseClone();
            result.clipList = new List<ClipInfo>();
            foreach (ClipInfo clipInfo in clipList)
            {
                result.clipList.Add(clipInfo.Clone());
            }
            return result;
        }
    }

    [System.Serializable]
    public class IdleInfo : InfoBase
    {
        public float interval = 15;
        public float random = 0.5f;
        public AnimationClip defaultAnimClip;

        public IdleInfo Clone()
        {
            IdleInfo result = (IdleInfo) MemberwiseClone();
            result.clipList = new List<ClipInfo>();
            foreach (ClipInfo clipInfo in clipList)
            {
                result.clipList.Add(clipInfo.Clone());
            }
            return result;
        }
    }

    [System.Serializable]
    public class ApproachInfo : InfoBase
    {
        public float radius = 1.0f;

        public ApproachInfo Clone()
        {
            ApproachInfo result = (ApproachInfo) MemberwiseClone();
            result.clipList = new List<ClipInfo>();
            foreach (ClipInfo clipInfo in clipList)
            {
                result.clipList.Add(clipInfo.Clone());
            }
            return result;
        }
    }

    [System.Serializable]
    public class InfoBase
    {
        public List<ClipInfo> clipList = new List<ClipInfo>();
        public bool autoRandom = true;
    }

    [System.Serializable]
    public class ClipInfo
    {
        private const float CONST_RANDOM = 1.0f;

        public float random = CONST_RANDOM;
        public bool lookFlag = true;
        public AnimationClip animClip;
        public AudioClip audioClip;

        public ClipInfo(AnimationClip _animClip) : this(_animClip, null) { }
        public ClipInfo(AudioClip _audioClip) : this(null, _audioClip) { }
        public ClipInfo(AnimationClip _animClip = null, AudioClip _audioClip = null, float _random = CONST_RANDOM)
        {
            animClip = _animClip;
            audioClip = _audioClip;
            random = _random;
        }

        public ClipInfo Clone()
        {
            return (ClipInfo) MemberwiseClone();
        }
    }

    [System.Serializable]
    public class ConfigInfo
    {
        public List<BoneInfo> boneInfoList = new List<BoneInfo>();
        public IdleInfo idleInfo;
        public ApproachInfo approachInfo;
        public EventObjects eventObjs;
        public DefaultAsset saveFolder;
        public string prefabName;

        public ConfigInfo Clone()
        {
            ConfigInfo result = (ConfigInfo) MemberwiseClone();
            result.boneInfoList = new List<BoneInfo>(boneInfoList);
            for (int i = 0; i < result.boneInfoList.Count; i++)
            {
                result.boneInfoList[i] = boneInfoList[i].Clone();
            }
            result.idleInfo = idleInfo.Clone();
            result.approachInfo = approachInfo.Clone();
            result.eventObjs = eventObjs.Clone();
            return result;
        }
    }

    [System.Serializable]
    public class ClipFold
    {
        public bool mainFoldFlag = false;
        public bool clipFoldFlag = true;
    }

    [System.Serializable]
    public class EventObjects
    {
        public GameObject prefabObj;
        public GameObject audioSourceObj;
        public GameObject eventsObj;
        public GameObject lookSwitcherObj;
        public GameObject trueObj;
        public GameObject falseObj;
        public GameObject textDebrisObj;
        public GameObject textObj;
        public GameObject idlesObj;
        public GameObject buttonsObj;
        public GameObject animClipsObj;
        public GameObject audioClipsObj;
        public GameObject playerTrackingObj;
        public GameObject[] boneColliderObjs = new GameObject[System.Enum.GetNames(typeof(HumanBodyBones)).Length];
        public GameObject approachColliderObj;
        public GameObject setupObj;

        public EventObjects Clone()
        {
            return (EventObjects) MemberwiseClone();
        }
    }
}
#endif